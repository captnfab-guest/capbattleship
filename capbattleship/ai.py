from random import choice

def gen_coords(width, height):
    return [ (x, y) for x in range(width) for y in range(height) ]

def filter_coords(coords, to_filter):
    return [ c for c in coords if c not in to_filter ]

def keep_coords(coords, to_keep):
    return [ c for c in coords if c in to_keep ]

def filter_diag(coords, f):
    return [ (x,y) for (x,y) in coords if (y%f + x)%f != 0 ]


def all_coords(board):
    return gen_coords(board.width, board.height)

def filter_hitmiss(coords, board):
    return filter_coords(coords, board.hits+board.miss)

def filter_sunk(coords, board):
    ships_coords = [ (x,y)
            for ship in board.sunk
            for x in range(max(ship.xmin-1, 0), min(ship.xmax+1, board.width))
            for y in range(max(ship.ymin-1, 0), min(ship.ymax+1, board.height))
            ]
    return filter_coords(coords, ships_coords)


def ship_found(player):
    l = []
    for ship in player.ships:
        if ship.get_health() > 0 and ship.get_health() < ship.size:
            for i in range(ship.size):
                if ship.health[i] == 0:
                    l.append(ship.coords_of(i))
    if len(l) == 0:
        return None
    l = sorted(l, key=lambda s: 10*s[0] + s[1])

    xmin, ymin = xmax, ymax = l[0]

    while (xmax + 1, ymax) in l:
        xmax += 1

    if xmin == xmax:
        while (xmax, ymax + 1) in l:
            ymax +=1

    return (xmin, xmax, ymin, ymax)


def interesting_coords(player):
    i = ship_found(player)
    if i is None:
        return []
    else:
        (xmin, xmax, ymin, ymax) = i
        if xmin < xmax:
            y = ymin
            if xmin==0 or (xmin-1,y) in player.miss:
                x = xmax+1
                return [(x, y)]
            elif xmax==player.width-1 or (xmax+1, y) in player.miss:
                x = xmin-1
                return [(x, y)]
            else:
                xs = [xmin-1, xmax+1]
                return [(x, y) for x in xs]
        elif ymin < ymax:
            x = xmin
            if ymin==0 or (x,ymin-1) in player.miss:
                y = ymax+1
                return [(x, y)]
            elif ymax==player.width-1 or (x, ymax+1) in player.miss:
                y = ymin-1
                return [(x, y)]
            else:
                ys = [ymin-1, ymax+1]
                return [(x, y) for y in ys]
        else:
            p = [(xmin-1, ymin), (xmin+1, ymin), (xmin, ymin-1), (xmin, ymin+1)]
            l = []
            for c in p:
                if c not in player.miss and all([c[i]>=0 and c[i]<player.width for i in [0, 1]]):
                    l.append(c)
            return l

def ai_cheat(player):
    c = all_coords(player)
    c = filter_hitmiss(c, player)
    g = [ ship.coords_of(i)
            for ship in player.ships
            for i in range(ship.size)
            if ship.health[i] > 0
            ]
    c = filter_coords(c, g)
    if len(c) > 0:
        return choice(c)
    else:
        return choice(g)

def ai_easy(player):
    c = all_coords(player)
    c = filter_hitmiss(c, player)
    return choice(c)

def ai_normal(player):
    c = all_coords(player)
    c = filter_hitmiss(c, player)
    c = filter_sunk(c, player)
    i = interesting_coords(player)
    if len(i) > 0:
        c = keep_coords(c, i)
    return choice(c)

def ai_hard(player):
    c = all_coords(player)
    c = filter_hitmiss(c, player)
    c = filter_sunk(c, player)
    i = interesting_coords(player)
    if len(i) > 0:
        c = keep_coords(c, i)
    else:
        c = filter_diag(c, 2)
    return choice(c)

def ai_insane(player):
    c = [ ship.coords_of(i)
            for ship in player.ships
            for i in range(ship.size)
            if ship.health[i] > 0
            ]
    return choice(c)
