��    D      <  a   \      �     �     �               
       
   .     9     J     N     f     s     �     �  )   �     �     �     �     �       
             (     -  �   5  
   �  6   �     6     ;     ?  /   F     v     �  	   �     �  %   �     �     �     �  	   �     �  �   �     �	     �	     �	     �	     �	  D   �	     
  1   ,
     ^
      c
  	   �
     �
     �
     �
  �   �
  �   x     C     Q     V     _     n     |     �  
   �     �  �  �     �     �     �     �     �  %   �  
   �            "      (   C  !   l     �  *   �  +   �     �     �          +     4     K     W     d  	   k  �   u     \  P   m  	   �     �     �  5   �          "     (     7  *   ?     j     r     �     �     �  �   �     �     �     �     �     �  M   �       3   2  	   f      p  	   �     �     �     �  �   �  �   �     l     {  
   �  
   �     �  #   �  #   �  	   �  %        )      ;       '          +   B   7   .         3                 2   "          *   =         1   -   :                  A      
                 ?             9   	       0      %                           >       $                  &   <   #   5   8   /      ,   D          @       C   6   !       (   4                                                   0.5s 100 Golden Hern piece (Coins) 1s 2s All the testers Already shot there. Animations Background Music CAP CAP - Pirate Battleship CPU hit you. CPU missed you. CPU starts. CPU sunk you. Canon Gun - Canon sound_01 (sound effect) Cheat Choose your pirate Container Pack (Barrels) Continue Continue the game Difficulty Disabled Easy Enabled Every shipman is only grousing day and night, which drives the captain completely mad. There is one thing you can do to help him unwind, that is be an easy pray. I would advise you not to… Fullscreen Graphism, game design (illustrations, ships, VFX, Map) Hard Hit Insane Join us on https://capbattleship.tuxfamily.org/ Manage options Menu Menu only Miss Mr. Bumble (Inspiration for 3D model) Normal Notification speed Pirate Battleship Play once Quit Rather than the vessel, 'The Princess' refers to its heartless captain. Famous buccaneer, she is said to be as rich as a king. Well, if she meets you, maybe she well become even richer… Repeat See credits Sfx Shots Skip Source code development (uses python3, pygame), translation (fr, en) Start the game Stone in the water 080117-002 drop (sound effect) Sunk Thanks! (testing, advices, bugs) The Anger The Pirates Storm The Princess The Victory Saber They were twin brothers, incredibly talentuous swashbucklers, and the fell for a mysterious lady. She said only the most fearsome pirate would be her match. Will you try their cruelty? This vessel is known for its sneaky attacks during storms. Manœuvrable and fast, no ship can escape her in the raging waters. If the weather is taking a turn for the worse, be sure that you're alone… Titan (Music) Wait Windowed You have lost. You have won. You hit CPU. You missed CPU. You start. You sunk CPU. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: contact@capbattleship.tuxfamily.org
PO-Revision-Date: 2020-12-05 09:29+0000
Last-Translator: J. Lavoie <j.lavoie@net-c.ca>
Language-Team: Italian <https://hosted.weblate.org/projects/capbattleship/capbattleship/it/>
Language: it
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.4-dev
Generated-By: pygettext.py 1.5
 0,5 s 100 Golden Hern piece (monete) 1 s 2 s Tutti i collaudatori Capitano, abbiamo già sparato lì… Animazioni Musica di sottofondo CAP CAP — La battaglia navale pirata Il nemico ha colpito una delle tue navi. Il nemico ha mancato le tue navi. Il nemico inizia. Il nemico ha affondato una delle tue navi. Canon Gun - Canon sound_01 (effetto sonoro) Barare Scegli il tuo pirata Container Pack (Barili) Continua Riprendi la battaglia! Difficoltà Disabilitato Facile Abilitato Ogni marinaio non fa altro che gongolare giorno e notte, il che fa impazzire completamente il capitano. C'è solo una cosa che si può fare per aiutarlo a rilassarsi, e sarebbe essere una facile preda. Ti consiglio di non farlo… A schermo intero Grafica digitale, design del gioco (illustrazioni, navi, effetti grafici, mappa) Difficile Colpito Demente Unisciti a noi : https://capbattleship.tuxfamily.org/ Gestisci le opzioni Menù Menù di gioco Mancato Mr. Bumble (ispirazione per il modello 3D) Normale Velocità delle notifiche La battaglia navale pirata Gioca una volta Torna sulla terraferma Piuttosto che la nave, La Principessa si riferisce al suo capitano senza cuore. Famosa bucaniera, si dice che sia ricca come un re. Beh, se ti incontra forse diventerà ancora più ricca… Ripeti Vedi crediti Effetti sonori Palle di cannone sparate Ignora Sviluppo del codice sorgente (utilizza Python 3, pygame), traduzione (fr, en) Inizia la battaglia! Stone in the water 080117-002 drop (effetto sonoro) Affondato Grazie! (test, consigli, errori) La rabbia La tempesta dei pirati La Principessa La sciabola della vittoria Erano fratelli gemelli, spadaccini incredibilmente talentuosi e innamorati di una donna misteriosa. Disse che solo il pirata più temibile sarebbe stato il suo compagno. Proverai la loro crudeltà? Questa nave è nota per i suoi attacchi subdoli durante le tempeste. Manovrabile e veloce, nessuna nave può sfuggirle nelle acque impetuose. Se il tempo sta peggiorando, assicurati di essere solo/a… Titan (musica) Aspetta Finestrato Hai perso. Congratulazioni, hai vinto. Hai colpito una delle navi nemiche. Hai colpito una delle navi nemiche. Tu inizi. Hai affondato una delle navi nemiche. 