from random import randrange, choice
from enum import Enum, auto

from .ai import ai_cheat, ai_easy, ai_normal, ai_hard, ai_insane

class Direction(Enum):
    NORTH = auto()
    SOUTH = auto()
    EAST = auto()
    WEST = auto()

class Outcome(Enum):
    MISS = auto()
    HIT = auto()
    SUNK = auto()

class AI(Enum):
    CHEAT = auto()
    EASY = auto()
    NORMAL = auto()
    HARD = auto()
    INSANE = auto()

class BgRepeat(Enum):
    NONE=auto()
    MENU=auto()
    ONCE=auto()
    LOOP=auto()

class Ship:
    def __init__(self, x, y, direction, size, name=""):
        self.x = x
        self.y = y
        self.size = size
        self.health = [1] * size
        self.direction = direction
        self.name = name

        self.ymin = y
        self.ymax = y
        self.xmin = x
        self.xmax = x
        if self.direction == Direction.EAST:
            self.xmax = x + (size - 1)
        elif self.direction == Direction.WEST:
            self.xmin = x - (size - 1)
        elif self.direction == Direction.SOUTH:
            self.ymax = y + (size - 1)
        elif self.direction == Direction.NORTH:
            self.ymin = y - (size - 1)

    def _pos_shoot(self,x,y):
        """Return shot position or None"""
        pos = None
        if x >= self.xmin and x <= self.xmax and y >= self.ymin and y <= self.ymax:
            pos = y-self.ymin + x-self.xmin
            if self.direction == Direction.WEST or self.direction == Direction.NORTH:
                pos = (self.size -1) - pos
        return pos

    def coords_of(self, i):
        """Return position of index or None"""
        if i < 0 or i >= self.size or self.direction is None:
            return None

        if self.direction == Direction.WEST:
            return (self.x-i, self.y)
        elif self.direction == Direction.EAST:
            return (self.x+i, self.y)
        elif self.direction == Direction.NORTH:
            return (self.x, self.y-i)
        elif self.direction == Direction.SOUTH:
            return (self.x, self.y+i)

    def _damage_pos(self, pos):
        """Decrease health of position"""
        if self.health[pos] > 0:
            self.health[pos] -= 1

    def get_health(self):
        """Return ship health"""
        return sum(self.health)

    def shoot(self, x, y):
        """Return Outcome for ship after shooting at x,y"""
        pos = self._pos_shoot(x, y)
        if pos is not None:
            old = self.get_health()
            self._damage_pos(pos)
            new = self.get_health()
            if new < old:
                if new > 0:
                    return Outcome.HIT
                else:
                    return Outcome.SUNK
            else:
                return Outcome.MISS

    def is_overlap(self, ship):
        """Return wether ship overlaps with self or not"""
        for x in range(min(self.xmin, self.xmax)-1, max(self.xmin,self.xmax)+1+1):
            for y in range(min(self.ymin, self.ymax)-1, max(self.ymin, self.ymax)+1+1):
                if ship._pos_shoot(x, y) is not None:
                    return True
        return False


class PlayerBoard:
    def __init__(self, width, height, is_player):
        self.width = width
        self.height = height
        self.ships = []
        self.hits = []
        self.miss = []
        self.sunk = []
        self.character = None
        self.is_player = is_player

    def add_ship(self, x, y, direction, size, name=""):
        ship = Ship(x, y, direction, size, name)
        if ship.xmax >= self.width or ship.xmin < 0 or ship.ymax >= self.height or ship.ymin < 0:
            raise ValueError

        if any([ship.is_overlap(s) for s in self.ships]):
            raise ValueError

        self.ships.append(ship)

    def shoot(self, x, y):
        if x < 0 or x >= self.width or y < 0 or y >= self.height:
            raise ValueError
        for ship in self.ships:
            outcome = ship.shoot(x,y)
            if outcome == Outcome.SUNK:
                self.hits.append((x,y))
                self.sunk.append(ship)
                return outcome
            elif outcome == Outcome.HIT:
                self.hits.append((x,y))
                return outcome
        self.miss.append((x,y))
        return Outcome.MISS

class Round:
    class State:
        stage=None
        options=None

        class Stage(Enum):
            MENU = auto()
            OPTIONS = auto()
            CREDITS = auto()
            NEWGAME = auto()
            CHARACTERS = auto()
            PLAYER_SHIPS = auto()
            CPU_SHIPS = auto()
            PLAYER_TURN = auto()
            CPU_TURN = auto()
            NOTIFICATION = auto()
            GAMEOVER = auto()
            QUIT = auto()

        def __init__(self, stage, options={}):
            self.stage=stage
            self.options=options

        def switch(self, state_type, options={}, notification=None, save=False, restore=False, reset=False):
            old_stage = self.options.get('old_stage', None)
            old_options = self.options.get('old_options', None)
            if reset:
                old_stage = None
                old_options = None

            if restore and old_stage is not None:
                self.stage=old_stage
                self.options.clear()
                self.options=old_options
                self.options['needs_redraw']=True
                self.options['old_stage']=None
                self.options['old_options']=None
            elif save:
                old_stage = self.stage
                old_options = self.options.copy()
                self.stage=state_type
                self.options.clear()
                self.options=options.copy()
                self.options['old_stage'] = old_stage
                self.options['old_options'] = old_options
            elif notification is not None:
                self.stage=self.Stage.NOTIFICATION
                self.options.clear()
                self.options=options.copy()
                self.options['next_stage']=state_type
                self.options['next_options']=options
                self.options['message']=notification
                self.options['old_stage'] = old_stage
                self.options['old_options'] = old_options
            else:
                self.stage=state_type
                self.options.clear()
                self.options=options.copy()
                self.options['old_stage'] = old_stage
                self.options['old_options'] = old_options


    def __init__(self, width, ui, tui=None):
        if width > 26:
            raise ValueError
        self.width = width
        self.quit = False
        self.ui = ui
        self.textui = tui
        self.state = self.State(self.State.Stage.MENU)
        self.show_credits = True
        self.ships = [
            ("The Anger", 2),
            ("The Victory Saber", 3),
            ("The Victory Saber", 3),
            ("The Pirate Storm", 4),
            ("The Princess", 5),
            ]
        self.ships.reverse()
        self.player = None
        self.cpu = None
        self.characters = [
                ("Ethel Bonny", "ethel-bonny"),
                ("Billy Wright", "bill-wright"),
                ("Greaves Black Beard","greaves"),
                #("Damien Monbars", "bill-wright"),
                ]

        self.params = sync_config(None)
        try:
            self.params = sync_config(None)
        except:
            self.params = {}

        for entry,default in {
                "ai": AI.NORMAL,
                "notif_timeout": 2000,
                "music": BgRepeat.ONCE,
                "sfx": True,
                "animations": True,
                }.items():
            self.params[entry] = self.params.get(entry, default)


    def update(self):
        if self.state.stage == self.State.Stage.QUIT:
            self.quit = True
        elif self.state.stage == self.State.Stage.MENU:
            next_stage = self.state.options.get('choice', None)
            next_stage_opt = self.state.options.get('opt', {})
            old_stage = self.state.options.get('old_stage', None)
            if next_stage is self.State.Stage.NEWGAME and old_stage is not None:
                self.state.switch(next_stage, **next_stage_opt)
            elif next_stage is not None:
                self.state.switch(next_stage)
        elif self.state.stage in [self.State.Stage.OPTIONS, self.State.Stage.CREDITS]:
            next_stage = self.state.options.get('choice', None)
            if next_stage is not None:
                self.state.switch(next_stage)
                sync_config(self.params)
        elif self.state.stage == self.State.Stage.NEWGAME:
            self.cpu = PlayerBoard(self.width, self.width, is_player=False)
            self.player = PlayerBoard(self.width, self.width, is_player=True)
            self.state.switch(self.State.Stage.CHARACTERS)
        elif self.state.stage == self.State.Stage.CHARACTERS:
            if self.state.options.get('choice', None) is not None:
                cc = self.state.options['choice']
                if cc >= 0 and cc < len(self.characters):
                    self.player.character = cc
                    self.cpu.character = choice([ c for c in range(len(self.characters)) if c != self.player.character ])
                    self.state.switch(self.State.Stage.PLAYER_SHIPS)
                else:
                    self.state.options['choice'] = None
        elif self.state.stage == self.State.Stage.GAMEOVER:
            if self.state.options.get('skip', False):
                if self.show_credits == True:
                    self.state.switch(self.State.Stage.CREDITS)
                    self.show_credits = False
                else:
                    self.state.switch(self.State.Stage.MENU)
        elif self.state.stage == self.State.Stage.NOTIFICATION:
            if self.state.options.get('skip', False):
                self.state.switch(self.state.options['next_stage'], self.state.options['next_options'])
        elif self.state.stage == self.State.Stage.PLAYER_SHIPS:
            if self.state.options.get('ready', False) and self.state.options.get('x', None) is not None and self.state.options.get('y', None) is not None and self.state.options.get('direction', None) is not None:
                (shipname, shiplen) = self.ships[len(self.player.ships)]
                try:
                    self.player.add_ship(self.state.options['x'], self.state.options['y'], self.state.options['direction'], shiplen, shipname)
                except ValueError:
                    pass
                finally:
                    self.state.options = {}
            elif self.state.options.get('auto', False):
                valid_pos = False
                (shipname, shiplen) = self.ships[len(self.player.ships)]
                while not valid_pos:
                    try:
                        x,y = randrange(0, self.cpu.width), randrange(0, self.cpu.height)
                        self.player.add_ship(x, y, choice([Direction.NORTH, Direction.SOUTH, Direction.WEST, Direction.EAST]), shiplen, shipname)
                        valid_pos = True
                    except ValueError:
                        pass
                self.state.options.clear()

            if len(self.player.ships) == len(self.ships):
                self.state.switch(self.State.Stage.CPU_SHIPS)

        elif self.state.stage == self.State.Stage.CPU_SHIPS:
            while len(self.cpu.ships) < len(self.ships):
                x = randrange(0, self.cpu.width)
                y = randrange(0, self.cpu.height)
                direction = choice([Direction.NORTH, Direction.SOUTH, Direction.WEST, Direction.EAST])

                try:
                    (shipname, shiplen) = self.ships[len(self.cpu.ships)]
                    self.cpu.add_ship(x, y, direction, shiplen, shipname)
                except ValueError:
                    pass
            first = choice([self.State.Stage.PLAYER_TURN,self.State.Stage.CPU_TURN])
            if first == self.State.Stage.PLAYER_TURN:
                self.state.switch(first, options={ "message": "You start." })
            else:
                self.state.switch(first, notification="CPU starts.")

        elif self.state.stage == self.State.Stage.PLAYER_TURN:
            if self.state.options.get('auto', False):
                while (self.state.options.get('x', None) is None and self.state.options.get('y', None) is None) or (self.state.options['x'],self.state.options['y']) in self.cpu.miss or (self.state.options['x'],self.state.options['y']) in self.cpu.hits:
                    self.state.options['x'] = randrange(0, self.cpu.width)
                    self.state.options['y'] = randrange(0, self.cpu.height)

            if (self.state.options.get('ready', False) or self.state.options.get('auto', False))  and self.state.options.get('x', None) is not None and self.state.options.get('y', None) is not None:
                if (self.state.options['x'], self.state.options['y']) in self.cpu.hits or (self.state.options['x'], self.state.options['y']) in self.cpu.miss:
                    self.state.options['ready'] = False
                    self.state.options['message'] = "Already shot there."
                else:
                    try:
                        outcome = self.cpu.shoot(self.state.options['x'], self.state.options['y'])
                        if outcome == Outcome.HIT:
                            self.state.switch(self.State.Stage.CPU_TURN, notification="You hit CPU.")
                        elif outcome == Outcome.MISS:
                            self.state.switch(self.State.Stage.CPU_TURN, notification="You missed CPU.")
                        elif len(self.cpu.sunk) == len(self.ships):
                            self.state.switch(self.State.Stage.GAMEOVER, options={'victory': True},  notification="You sunk CPU.")
                        else:
                            self.state.switch(self.State.Stage.CPU_TURN, notification="You sunk CPU.")
                    except ValueError:
                        pass
        elif self.state.stage == self.State.Stage.CPU_TURN:
            if self.params['ai'] == AI.CHEAT:
                x,y = ai_cheat(self.player)
            elif self.params['ai'] == AI.EASY:
                x,y = ai_easy(self.player)
            elif self.params['ai'] == AI.NORMAL:
                x,y = ai_normal(self.player)
            elif self.params['ai'] == AI.HARD:
                x,y = ai_hard(self.player)
            elif self.params['ai'] == AI.INSANE:
                x,y = ai_insane(self.player)

            outcome = self.player.shoot(x,y)

            if outcome == Outcome.HIT:
                self.state.switch(self.State.Stage.PLAYER_TURN, options={ "message": "CPU hit you."})
            elif outcome == Outcome.MISS:
                self.state.switch(self.State.Stage.PLAYER_TURN, options={ "message": "CPU missed you."})
            else:
                if len(self.player.sunk) == len(self.ships):
                    self.state.switch(self.State.Stage.GAMEOVER, options={ "message": "CPU sunk you.", "victory": False})
                else:
                    self.state.switch(self.State.Stage.PLAYER_TURN, options={ "message": "CPU sunk you."})

    def run(self):
        while not self.quit:
            self.update()
            self.ui.draw(self)
            self.textui.draw(self)
            self.ui.read_events(self)

def sync_config(data=None):
    from configparser import ConfigParser
    import os

    config = ConfigParser()

    xdgprefix = os.getenv('XDG_CONFIG_HOME', os.path.expanduser('~/.config'))
    dirname = os.path.join(xdgprefix, "capbattleship")
    os.makedirs(dirname, exist_ok=True)

    filename = os.path.join(dirname, "config")

    if data is not None:
        for (key, value) in data.items():
            config['DEFAULT'][key] = str(value)
        with open(filename, 'w') as configfile:
            config.write(configfile)
    elif os.path.isfile(filename):
        config.read(filename)
        data = {}
        for key in ["sfx", "animations"]:
            try:
                data[key] = config['DEFAULT'].getboolean(key)
            except:
                pass
        for key in ["notif_timeout"]:
            try:
                data[key] = config['DEFAULT'].getfloat(key)
            except:
                pass
        for key in ["fullscreen"]:
            try:
                data[key] = config['DEFAULT'].getinteger(key)
            except:
                pass
        try:
            data['ai'] = AI.__members__[config['DEFAULT']['ai'][3:]]
        except:
            pass
        try:
            data['music'] = BgRepeat.__members__[config['DEFAULT']['music'][9:]]
        except:
            pass

        return data
    else:
        return {}
