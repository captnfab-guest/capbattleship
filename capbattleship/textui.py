from .logic import Direction

class DummyUI:
    def draw(self, rnd):
        pass
    def read_events(self, rnd):
        if rnd.state.stage == rnd.State.Stage.MENU:
            rnd.state.options['choice'] = rnd.State.Stage.PLAYER_SHIPS
        elif rnd.state.stage == rnd.State.Stage.CHARACTERS:
            rnd.state.options['choice'] = 0
        elif rnd.state.stage == rnd.State.Stage.GAMEOVER:
            rnd.state.options['skip'] = True
        elif rnd.state.stage == rnd.State.Stage.NOTIFICATION:
            rnd.state.options['skip'] = True
        elif rnd.state.stage == rnd.State.Stage.PLAYER_SHIPS:
            rnd.state.options['auto'] = True
        elif rnd.state.stage == rnd.State.Stage.CPU_SHIPS:
            pass
        elif rnd.state.stage == rnd.State.Stage.PLAYER_TURN:
            rnd.state.options['auto'] = True
        elif rnd.state.stage == rnd.State.Stage.CPU_TURN:
            pass

def coords_l2n(coords):
    p = coords.lower()
    if len(p) >= 2 and p[0].isalpha() and p[1:].isdecimal():
        x = ord(p[0]) - ord('a')
        y = int(p[1:]) - 1
        return (x,y)
    return None

def direction_l2e(direction):
    d = direction.lower()
    if d in [ "nord", "north", "n", "haut", "h" ]:
        direction = Direction.NORTH
    elif d in [ "sud", "south", "s", "bas", "b" ]:
        direction = Direction.SOUTH
    elif d in [ "est", "east", "e", "droite", "d" ]:
        direction = Direction.EAST
    elif d in [ "ouest", "west", "o", "w", "gauche", "g" ]:
        direction = Direction.WEST
    else:
        direction = None
    return direction

class TextUI:
    class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKCYAN = '\033[96m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'

    def read_events(self, rnd):
        if rnd.state.stage in [rnd.State.Stage.MENU, rnd.State.Stage.CREDITS, rnd.State.Stage.OPTIONS, rnd.State.Stage.GAMEOVER, rnd.State.Stage.NOTIFICATION]:
            _ = input()
            rnd.state.options['skip'] = True
        elif rnd.state.stage == rnd.State.Stage.CHARACTERS:
            rnd.state.options['choice'] = input("> ")
        elif rnd.state.stage == rnd.State.Stage.PLAYER_SHIPS:
            if rnd.state.options.get('x', None) is None or rnd.state.options.get('y', None) is None:
                pos = input("> ")
                if pos == "":
                    rnd.state.options['auto'] = True
                else:
                    pos = coords_l2n(pos)
                    if pos is not None:
                        rnd.state.options['x'] = pos[0]
                        rnd.state.options['y'] = pos[1]
            elif rnd.state.options.get('direction', None) is None:
                direction = input("[N/S/E/W]> ")
                direction = direction_l2e(direction)
                if direction is not None:
                    rnd.state.options['direction'] = direction
                    rnd.state.options['ready'] = True
        elif rnd.state.stage == rnd.State.Stage.CPU_SHIPS:
            pass
        elif rnd.state.stage == rnd.State.Stage.PLAYER_TURN:
            if rnd.state.options.get('x', None) is None or rnd.state.options.get('y', None) is None:
                pos = input("> ")
                if pos == "":
                    rnd.state.options['auto'] = True
                else:
                    pos = coords_l2n(pos)
                    if pos is not None:
                        rnd.state.options['x'] = pos[0]
                        rnd.state.options['y'] = pos[1]
                        rnd.state.options['ready'] = True
        elif rnd.state.stage == rnd.State.Stage.CPU_TURN:
            pass

    def draw_ship(self, ship, grid):
        dead = ship.get_health() == 0
        for x in range(min(ship.xmin, ship.xmax), max(ship.xmin,ship.xmax)+1):
            for y in range(min(ship.ymin, ship.ymax), max(ship.ymin, ship.ymax)+1):
                if dead:
                    color = self.bcolors.FAIL
                elif ship.health[ship._pos_shoot(x,y)]==0:
                    color = self.bcolors.WARNING
                else:
                    color = self.bcolors.ENDC
                grid[y][x] = color + "X" + self.bcolors.ENDC

    def draw_playerboard(self, board, show_ships=True):
        grid = []
        for y in range(board.height):
            grid.append([' ']*board.width)

        for (mx, my) in board.miss:
            grid[my][mx] = self.bcolors.OKBLUE + 'X' + self.bcolors.ENDC

        if show_ships:
            for ship in board.ships:
                self.draw_ship(ship,grid)
        else:
            for (hx, hy) in board.hits:
                grid[hy][hx] = self.bcolors.WARNING + 'X' + self.bcolors.ENDC
            for ship in board.sunk:
                self.draw_ship(ship,grid)

        output = []
        output.append('   '+' '.join([chr(ord('A') + i) for i in range(board.width)])+' ')
        output.append('  ╔'+'╤'.join('═'*board.width)+'╗')
        first = True
        for (i,row) in enumerate(grid):
            if first:
                first = False
            else:
                output.append('  ╟'+'┼'.join('─'*board.width)+'╢')
            output.append(format(i+1," >2d")+'║'+'│'.join(row)+'║')
        output.append('  ╚'+'╧'.join('═'*board.width)+'╝')

        return output

    def draw(self, rnd):
        print('\033c')
        if rnd.state.stage == rnd.State.Stage.MENU:
            print("Cap Bataille Navale")
            print("")
            print("Appuyez sur une touche pour commencer")
        elif rnd.state.stage == rnd.State.Stage.OPTIONS:
            print("Les options sont changeables en mode graphique")
        elif rnd.state.stage == rnd.State.Stage.CREDITS:
            print("Les crédits sont visibles en mode graphique")
        elif rnd.state.stage == rnd.State.Stage.GAMEOVER:
            if rnd.state.options.get('victory', False):
                print("Partie perdue")
            else:
                print("Félicitations, vous avez gagné !")
        elif rnd.state.stage == rnd.State.Stage.PLAYER_SHIPS:
            print("Placement des bateaux")
            pb = self.draw_playerboard(rnd.player)
            print('\n'.join(pb) + '\n')
            print("Placez votre " + rnd.ships[len(rnd.player.ships)][0])
            if rnd.state.options.get('x',None) is None:
                print("Position de l'arrière du bateau (ex. C5): ")
            elif rnd.state.options.get('direction',None) is None:
                print("Direction du bateau (ex. Sud): ")
        elif rnd.state.stage == rnd.State.Stage.CPU_SHIPS:
            print("L'ordinateur place ses bâteaux.")
        elif rnd.state.stage == rnd.State.Stage.PLAYER_TURN:
            print("Torpillons l'enemi !")
            pb1 = self.draw_playerboard(rnd.player, show_ships=True)
            pb2 = self.draw_playerboard(rnd.cpu, show_ships=False)
            for i in range(len(pb1)):
                print(pb1[i] + "     " + pb2[i])
            print("hits:", len(rnd.cpu.hits))
            print("miss:", len(rnd.cpu.miss))
            print("Position à torpiller: ")
        elif rnd.state.stage == rnd.State.Stage.CPU_TURN:
            print("L'enemi attaque !")
            pb1 = self.draw_playerboard(rnd.player, show_ships=True)
            pb2 = self.draw_playerboard(rnd.cpu, show_ships=False)
            for i in range(len(pb1)):
                print(pb1[i] + "     " + pb2[i])
