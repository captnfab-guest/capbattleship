# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR ORGANIZATION
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2020-12-05 22:26+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: pygettext.py 1.5\n"


#: capbattleship/pygameui.py:46
msgid "CAP"
msgstr ""

#: capbattleship/pygameui.py:47
msgid "Pirate Battleship"
msgstr ""

#: capbattleship/pygameui.py:57
msgid "Restart the game"
msgstr ""

#: capbattleship/pygameui.py:58
msgid "Continue the game"
msgstr ""

#: capbattleship/pygameui.py:64
msgid "Start the game"
msgstr ""

#: capbattleship/pygameui.py:69
msgid "Manage options"
msgstr ""

#: capbattleship/pygameui.py:70
msgid "See credits"
msgstr ""

#: capbattleship/pygameui.py:71
msgid "Quit"
msgstr ""

#: capbattleship/pygameui.py:157 capbattleship/pygameui.py:501
msgid "Menu"
msgstr ""

#: capbattleship/pygameui.py:182
msgid "Difficulty"
msgstr ""

#: capbattleship/pygameui.py:183
msgid "Cheat"
msgstr ""

#: capbattleship/pygameui.py:184
msgid "Easy"
msgstr ""

#: capbattleship/pygameui.py:185
msgid "Normal"
msgstr ""

#: capbattleship/pygameui.py:186
msgid "Hard"
msgstr ""

#: capbattleship/pygameui.py:187
msgid "Insane"
msgstr ""

#: capbattleship/pygameui.py:190
msgid "Notification speed"
msgstr ""

#: capbattleship/pygameui.py:191
msgid "Wait"
msgstr ""

#: capbattleship/pygameui.py:192
msgid "2s"
msgstr ""

#: capbattleship/pygameui.py:193
msgid "1s"
msgstr ""

#: capbattleship/pygameui.py:194
msgid "0.5s"
msgstr ""

#: capbattleship/pygameui.py:195
msgid "Skip"
msgstr ""

#: capbattleship/pygameui.py:198
msgid "Background Music"
msgstr ""

#: capbattleship/pygameui.py:199 capbattleship/pygameui.py:206
#: capbattleship/pygameui.py:211 capbattleship/pygameui.py:216
msgid "Disabled"
msgstr ""

#: capbattleship/pygameui.py:200
msgid "Menu only"
msgstr ""

#: capbattleship/pygameui.py:201
msgid "Play once"
msgstr ""

#: capbattleship/pygameui.py:202
msgid "Repeat"
msgstr ""

#: capbattleship/pygameui.py:205
msgid "Sfx"
msgstr ""

#: capbattleship/pygameui.py:207 capbattleship/pygameui.py:212
#: capbattleship/pygameui.py:218
msgid "Enabled"
msgstr ""

#: capbattleship/pygameui.py:210
msgid "Animations"
msgstr ""

#: capbattleship/pygameui.py:215
msgid "Fullscreen"
msgstr ""

#: capbattleship/pygameui.py:217
msgid "Windowed"
msgstr ""

#: capbattleship/pygameui.py:405
msgid "Source code development (uses python3, pygame), translation (fr, en)"
msgstr ""

#: capbattleship/pygameui.py:411
msgid "Graphism, game design (illustrations, ships, VFX, Map)"
msgstr ""

#: capbattleship/pygameui.py:417
msgid "100 Golden Hern piece (Coins)"
msgstr ""

#: capbattleship/pygameui.py:423
msgid "Mr. Bumble (Inspiration for 3D model)"
msgstr ""

#: capbattleship/pygameui.py:428
msgid "Container Pack (Barrels)"
msgstr ""

#: capbattleship/pygameui.py:434
msgid "Titan (Music)"
msgstr ""

#: capbattleship/pygameui.py:440
msgid "Stone in the water 080117-002 drop (sound effect)"
msgstr ""

#: capbattleship/pygameui.py:446
msgid "Canon Gun - Canon sound_01 (sound effect)"
msgstr ""

#: capbattleship/pygameui.py:451
msgid "All the testers"
msgstr ""

#: capbattleship/pygameui.py:452
msgid "Thanks! (testing, advices, bugs)"
msgstr ""

#: capbattleship/pygameui.py:455
msgid "Contribute!"
msgstr ""

#: capbattleship/pygameui.py:456
msgid "Join us on https://capbattleship.tuxfamily.org/"
msgstr ""

#: capbattleship/pygameui.py:624
msgid "Choose your pirate"
msgstr ""

#: capbattleship/pygameui.py:896 capbattleship/pygameui.py:1313
msgid "Every shipman is only grousing day and night, which drives the captain completely mad. There is one thing you can do to help him unwind, that is be an easy pray. I would advise you not to…"
msgstr ""

#: capbattleship/pygameui.py:896 capbattleship/pygameui.py:1313
msgid "The Anger"
msgstr ""

#: capbattleship/pygameui.py:897 capbattleship/pygameui.py:1314
msgid "The Victory Saber"
msgstr ""

#: capbattleship/pygameui.py:897 capbattleship/pygameui.py:1314
msgid "They were twin brothers, incredibly talentuous swashbucklers, and the fell for a mysterious lady. She said only the most fearsome pirate would be her match. Will you try their cruelty?"
msgstr ""

#: capbattleship/pygameui.py:898 capbattleship/pygameui.py:1315
msgid "The Pirates Storm"
msgstr ""

#: capbattleship/pygameui.py:898 capbattleship/pygameui.py:1315
msgid "This vessel is known for its sneaky attacks during storms. Manœuvrable and fast, no ship can escape her in the raging waters. If the weather is taking a turn for the worse, be sure that you're alone…"
msgstr ""

#: capbattleship/pygameui.py:899 capbattleship/pygameui.py:1316
msgid "Rather than the vessel, 'The Princess' refers to its heartless captain. Famous buccaneer, she is said to be as rich as a king. Well, if she meets you, maybe she well become even richer…"
msgstr ""

#: capbattleship/pygameui.py:899 capbattleship/pygameui.py:1316
msgid "The Princess"
msgstr ""

#: capbattleship/pygameui.py:1304 capbattleship/pygameui.py:1727
msgid "Shots"
msgstr ""

#: capbattleship/pygameui.py:1305 capbattleship/pygameui.py:1729
msgid "Miss"
msgstr ""

#: capbattleship/pygameui.py:1306 capbattleship/pygameui.py:1728
msgid "Hit"
msgstr ""

#: capbattleship/pygameui.py:1307 capbattleship/pygameui.py:1730
msgid "Sunk"
msgstr ""

#: capbattleship/pygameui.py:1687
msgid "You have won."
msgstr ""

#: capbattleship/pygameui.py:1689
msgid "You have lost."
msgstr ""

#: capbattleship/pygameui.py:1742
msgid "Continue"
msgstr ""

#: capbattleship/pygameui.py:1822
msgid "CAP - Pirate Battleship"
msgstr ""

#: capbattleship/pygameui.py:1830
msgid "CPU hit you."
msgstr ""

#: capbattleship/pygameui.py:1831
msgid "CPU missed you."
msgstr ""

#: capbattleship/pygameui.py:1832
msgid "CPU sunk you."
msgstr ""

#: capbattleship/pygameui.py:1833
msgid "You hit CPU."
msgstr ""

#: capbattleship/pygameui.py:1834
msgid "You missed CPU."
msgstr ""

#: capbattleship/pygameui.py:1835
msgid "You sunk CPU."
msgstr ""

#: capbattleship/pygameui.py:1836
msgid "Already shot there."
msgstr ""

#: capbattleship/pygameui.py:1837
msgid "CPU starts."
msgstr ""

#: capbattleship/pygameui.py:1838
msgid "You start."
msgstr ""

