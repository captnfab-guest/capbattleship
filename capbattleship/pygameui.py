import math
import gettext
import locale
from enum import Enum, auto
from glob import glob
from random import choice

from .pygamesprites import *
from .logic import Direction, Ship, Round, AI, BgRepeat
Stage = Round.State.Stage

WHITE = (255, 255, 255)
ORANGE = (255, 127, 0)
BLACK = (0, 0, 0)
LIGHTGRAY = (0xd9, 0xd9, 0xd9)
MEDIUMGRAY = (0x88, 0x88, 0x88)
DARKGRAY = (0x5b, 0x5b, 0x5e)
BLUEGRAY = (0x39, 0x47, 0x57)
LIGHTBLUEGRAY = (0xa5, 0xa7, 0xac)

FONT="fonts/DejaVuSans.ttf"
WIDTH=1280
HEIGHT=720

def ts(*l):
    return tuple([sum(t) for t in zip(*l)])

def cut_text(width, text):
    out = []
    while len(text) > width:
        i = text[:width].rindex(" ")
        out.append(text[:i])
        text = text[i+1:]
    out.append(text)
    return out


class GuiMenu:
    def __init__(self, rnd):
        self.rnd = rnd

        # Precompute background
        self.bg = pygame.image.load("gfx/splashscreen.jpg").convert()
        title_font = pygame.font.Font(FONT, 35)
        subtitle_font = pygame.font.Font(FONT, 18)
        title = title_font.render(_("CAP"), 1, WHITE)
        subtitle = subtitle_font.render(_("Pirate Battleship"), 1, WHITE)
        self.bg.blit(title, (20, 15))
        self.bg.blit(subtitle, (20, 70))

        # Precompute menu
        self.menu_entries = []

        ## First meny entry is either sart or resume game
        if self.rnd.state.options.get('old_stage', None) is not None:
            self.menu_entries += [
                (_("Restart the game"),   'n', Stage.NEWGAME, {"reset": True}),
                (_("Continue the game"), 'r', Stage.NEWGAME, {"restore": True}),
                ]
            # Initially select resume game
            self.menu_index = 1
        else:
            self.menu_entries += [
                (_("Start the game"),  'n', Stage.NEWGAME, {"restore": False}),
                ]
            # Initially select new game
            self.menu_index = 0
        self.menu_entries += [
                (_("Manage options"),  'o', Stage.OPTIONS, {}),
                (_("See credits"),     'c', Stage.CREDITS, {}),
                (_("Quit"),            'q', Stage.QUIT, {}),
                ]


        ## Reference point for menu
        menu_topleft = (int(WIDTH*0.7), int(HEIGHT*0.7))

        ## Font face/size for menu entries
        menu_font = pygame.font.Font(FONT, 20)

        ## Generate menu's sprites
        self.menu_sprites = []
        for n,(text, key, stage, opt) in enumerate(self.menu_entries):
            active   = menu_font.render(text, 1, ORANGE)
            inactive = menu_font.render(text, 1, WHITE)

            rect = active.get_rect()
            rect.topleft = menu_topleft
            rect.left += n*20
            rect.top  += n*(10+rect.height)

            self.menu_sprites.append((rect, inactive, active))

    def update(self):
        pass

    def draw(self, s):
        update_rects = []

        # Draw background
        s.blit(self.bg, (0, 0))

        # Draw menu
        for (n, (rect, inactive, active)) in enumerate(self.menu_sprites):
            if self.menu_index == n:
                item = active
            else:
                item = inactive
            s.blit(item, rect)
            update_rects.append(rect)

        return update_rects

    def input(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            for n, ((rect,inactive,active), (text,key,stage,opt)) in enumerate(zip(self.menu_sprites, self.menu_entries)):
                if rect.collidepoint(event.pos):
                    self.rnd.state.options['choice'] = stage
                    self.rnd.state.options['opt'] = opt
                    self.rnd.state.options['needs_redraw'] = True
        elif event.type == pygame.KEYDOWN:
            for n, (text,key,stage,opt) in enumerate(self.menu_entries):
                if event.unicode == key:
                    self.rnd.state.options['choice'] = stage
                    self.rnd.state.options['opt'] = opt
                    self.rnd.state.options['needs_redraw'] = True
            if event.key == pygame.K_DOWN:
                self.menu_index += 1
                self.menu_index %= len(self.menu_entries)
                self.rnd.state.options['needs_redraw'] = True
            elif event.key == pygame.K_UP:
                self.menu_index += len(self.menu_entries)-1
                self.menu_index %= len(self.menu_entries)
                self.rnd.state.options['needs_redraw'] = True
            elif event.key == pygame.K_RETURN:
                if self.menu_index is not None:
                    self.rnd.state.options['choice'] = self.menu_entries[self.menu_index][2]
                    self.rnd.state.options['opt'] = self.menu_entries[self.menu_index][3]
                    self.rnd.state.options['needs_redraw'] = True
        elif event.type == pygame.MOUSEMOTION:
            for n, ((rect,inactive,active), (text,key,stage,opt)) in enumerate(zip(self.menu_sprites, self.menu_entries)):
                if rect.collidepoint(event.pos):
                    self.menu_index = n
                    self.rnd.state.options['needs_redraw'] = True

class GuiOptions:
    def __init__(self, rnd):
        self.rnd = rnd

        # Precompute background
        self.bg = pygame.image.load("gfx/options.jpg").convert()

        # Precompute menu
        self.menu_entries = []

        self.menu_entries.append(
                (_("Menu"), 'm', Stage.MENU, {})
                )

        ## Reference point for menu
        menu_center = (WIDTH//2, int(HEIGHT*0.9))

        ## Font face/size for menu entries
        menu_font = pygame.font.Font(FONT, 20)

        ## Generate menu's sprites
        self.menu_sprites = []
        for n,(text, key, stage, opt) in enumerate(self.menu_entries):
            active   = menu_font.render(text, 1, ORANGE)
            inactive = menu_font.render(text, 1, WHITE)

            rect = active.get_rect()
            rect.center = menu_center

            self.menu_sprites.append((rect, inactive, active))

        # Initially don't select any entry
        self.menu_index = None

        # Precompute options menu
        self.options_entries = [
                (_("Difficulty"), [
                    (_("Cheat"),  AI.CHEAT),
                    (_("Easy"),   AI.EASY),
                    (_("Normal"), AI.NORMAL),
                    (_("Hard"),   AI.HARD),
                    (_("Insane"), AI.INSANE),
                    ],
                    "ai"),
                (_("Notification speed"), [
                    (_("Wait"),   float('inf')),
                    (_("2s"),     2000),
                    (_("1s"),     1000),
                    (_("0.5s"),    500),
                    (_("Skip"),      0),
                    ],
                    "notif_timeout"),
                (_("Background Music"), [
                    (_("Disabled"),  BgRepeat.NONE),
                    (_("Menu only"), BgRepeat.MENU),
                    (_("Play once"), BgRepeat.ONCE),
                    (_("Repeat"),    BgRepeat.LOOP),
                    ],
                    "music"),
                (_("Sfx"), [
                    (_("Disabled"), False),
                    (_("Enabled"),  True),
                    ],
                    "sfx"),
                (_("Animations"), [
                    (_("Disabled"), False),
                    (_("Enabled"),  True),
                    ],
                    "animations"),
                (_("Fullscreen"), [
                    (_("Disabled"), 0),
                    (_("Windowed"), 1),
                    (_("Enabled"), 2),
                    ],
                    "fullscreen"),
                ]

        ## Reference point for options
        options_topleft = (350, 100)

        ## Font face/size for options menu entries
        options_font = pygame.font.Font(FONT, 20)
        items_font   = pygame.font.Font(FONT, 18)

        ## Generate options menu's sprites
        self.options_sprites = []
        self.items_sprites = []
        max_option_width = 0
        for n,(o_text,items,o_name) in enumerate(self.options_entries):
            active = options_font.render(o_text, 1, ORANGE)
            inactive = options_font.render(o_text, 1, WHITE)

            o_rect = active.get_rect()
            o_rect.topleft = options_topleft
            o_rect.top += 60*n

            self.options_sprites.append((o_rect, inactive, active))
            self.items_sprites.append([])

            max_option_width = max(max_option_width, o_rect.width)

        for n,(o_text,items,o_name) in enumerate(self.options_entries):
            i_topleft = self.options_sprites[n][0].topleft
            i_topleft = i_topleft[0] + max_option_width, i_topleft[1]
            ### Generate option's items sprites
            for m,(i_text, i_val) in enumerate(items):
                a_active   = items_font.render(i_text, 1, ORANGE)
                a_inactive = items_font.render(i_text, 1, MEDIUMGRAY)
                i_active   = items_font.render(i_text, 1, LIGHTGRAY)
                i_inactive = items_font.render(i_text, 1, DARKGRAY)

                i_rect = a_active.get_rect()
                i_rect.topleft = i_topleft[0]+30, i_topleft[1]
                i_topleft = i_rect.topright

                self.items_sprites[n].append((i_rect, a_active, a_inactive, i_active, i_inactive))

        # Initially select first entry
        self.options_index = 0

        # Initially select configured entry
        self.items_index = []

        for n,option in enumerate(self.options_entries):
            for m,item in enumerate(option[1]):
                ## If item's value is the current configured option
                if item[1] == self.rnd.params.get(option[2], None):
                    self.items_index.append(m)
                    break
            ## fallback on first value
            if len(self.items_index)<=n:
                self.items_index.append(0)


    def draw(self, s):
        update_rects = []

        # Draw background
        s.blit(self.bg, (0, 0))

        # Draw options menu
        for n, ((o_rect,inactive,active),items) in enumerate(zip(self.options_sprites,self.items_sprites)):
            ## Update option entry
            if n == self.options_index:
                s.blit(active, o_rect)
            else:
                s.blit(inactive, o_rect)
            update_rects.append(o_rect)

            for m, (r, a_a, a_i, i_a, i_i) in enumerate(items):
                if n == self.options_index:
                    if self.items_index[n] == m:
                        s.blit(a_a, r)
                    else:
                        s.blit(a_i, r)
                else:
                    if self.items_index[n] == m:
                        s.blit(i_a, r)
                    else:
                        s.blit(i_i, r)
                update_rects.append(r)

        # Draw menu
        for (n, (rect, inactive, active)) in enumerate(self.menu_sprites):
            if self.menu_index == n:
                item = active
            else:
                item = inactive
            s.blit(item, rect)
            update_rects.append(rect)

        return update_rects

    def update(self):
        # Apply config options
        for (n, v) in enumerate(self.items_index):
            name = self.options_entries[n][2]
            self.rnd.params[name] = self.options_entries[n][1][v][1]

    def input(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            for n, ((rect,inactive,active), (text,key,stage,opt)) in enumerate(zip(self.menu_sprites, self.menu_entries)):
                if rect.collidepoint(event.pos):
                    self.rnd.state.options['choice'] = stage
                    self.rnd.state.options['opt'] = opt
            for n,items_sprites in enumerate(self.items_sprites):
                for m,(i_r, iaa, iai, iia, iii) in enumerate(items_sprites):
                    if i_r.collidepoint(event.pos):
                        self.items_index[n] = m
                        self.rnd.state.options['needs_redraw'] = True
        elif event.type == pygame.KEYDOWN:
            for n, (text,key,stage,opt) in enumerate(self.menu_entries):
                if event.unicode == key:
                    self.rnd.state.options['choice'] = stage
                    self.rnd.state.options['opt'] = opt
                    self.rnd.state.options['needs_redraw'] = True
            if event.key == pygame.K_RETURN:
                if self.menu_index is not None:
                    self.rnd.state.options['choice'] = self.menu_entries[self.menu_index][2]
                    self.rnd.state.options['opt'] = self.menu_entries[self.menu_index][3]
                    self.rnd.state.options['needs_redraw'] = True
            elif event.key == pygame.K_ESCAPE:
                    self.rnd.state.options['choice'] = Stage.MENU
                    self.rnd.state.options['opt'] = {}
                    self.rnd.state.options['needs_redraw'] = True
            elif event.key in [pygame.K_DOWN, pygame.K_UP, pygame.K_LEFT, pygame.K_RIGHT]:
                self.rnd.state.options['needs_redraw'] = True
                if event.key == pygame.K_DOWN:
                    if self.menu_index is not None:
                        self.menu_index += 1
                    elif self.options_index is not None:
                        self.options_index += 1
                elif event.key == pygame.K_UP:
                    if self.menu_index is not None:
                        self.menu_index -= 1
                    elif self.options_index is not None:
                        self.options_index -= 1
                elif event.key == pygame.K_LEFT and self.options_index is not None:
                    self.items_index[self.options_index] += len(self.options_entries[self.options_index][1]) - 1
                    self.items_index[self.options_index] %= len(self.options_entries[self.options_index][1])
                elif event.key == pygame.K_RIGHT and self.options_index is not None:
                    self.items_index[self.options_index] += 1
                    self.items_index[self.options_index] %= len(self.options_entries[self.options_index][1])
                if self.menu_index == len(self.menu_entries):
                    self.menu_index = None
                    self.options_index = 0
                elif self.menu_index == -1:
                    self.menu_index = None
                    self.options_index = len(self.options_entries)-1
                elif self.options_index in [-1, len(self.options_entries)]:
                    self.options_index = None
                    self.menu_index = 0
        elif event.type == pygame.MOUSEMOTION:
            for n, ((rect,inactive,active), (text,key,stage,opt)) in enumerate(zip(self.menu_sprites, self.menu_entries)):
                if rect.collidepoint(event.pos):
                    self.menu_index = n
                    self.options_index = None
                    self.rnd.state.options['needs_redraw'] = True
            for n, (o_r, oa, oi) in enumerate(self.options_sprites):
                if o_r.collidepoint(event.pos):
                    self.options_index = n
                    self.menu_index = None
                    self.rnd.state.options['needs_redraw'] = True
                for m, (i_r, iaa, iai, iia, iii) in enumerate(self.items_sprites[n]):
                    if i_r.collidepoint(event.pos):
                        self.options_index = n
                        self.menu_index = None
                        self.rnd.state.options['needs_redraw'] = True

class GuiCredits:
    def __init__(self, rnd):
        self.rnd = rnd

        # Precompute background
        self.bg  = pygame.image.load("gfx/credits.jpg").convert()

        self.quotes = [
                {
                    "author": "Fabien Givors (captnfab)",
                    "descr": _("Source code development (uses python3, pygame), translation (fr, en)"),
                    "url": "https://chezlefab.net",
                    "licence": "MIT",
                    },
                {
                    "author": "Damien Monteillard (lastrodamo)",
                    "descr": _("Graphism, game design (illustrations, ships, VFX, Map)"),
                    "url": "https://www.3dminfographie.com/",
                    "licence": "CC0",
                    },
                {
                    "author": "seeker47",
                    "descr": _("100 Golden Hern piece (Coins)"),
                    "url": "https://blendswap.com/blend/17578",
                    "licence": "CC0",
                    },
                {
                    "author": "'Kyd' (Joseph Clayton Clarke)",
                    "descr": _("Mr. Bumble (Inspiration for 3D model)"),
                    "url": "https://en.wikipedia.org/wiki/Bumble_(Oliver_Twist)",
                    },
                {
                    "author": "yd",
                    "descr": _("Container Pack (Barrels)"),
                    "url": "https://blendswap.com/blend/16194",
                    "licence": "CC0",
                    },
                {
                    "author": "Scott Buckley",
                    "descr": _("Titan (Music)"),
                    "url": "https://www.scottbuckley.com.au/",
                    "licence": "CC-BY 4.0",
                    },
                {
                    "author": "reklamacja",
                    "descr": _("Stone in the water 080117-002 drop (sound effect)"),
                    "url": "https://freesound.org/people/reklamacja/sounds/342457/",
                    "licence": "CC0",
                    },
                {
                    "author": "hellboy1305",
                    "descr": _("Canon Gun - Canon sound_01 (sound effect)"),
                    "url": "https://freesound.org/people/hellboy1305/sounds/370273/",
                    "licence": "CC0",
                    },
                {
                    "author": _("All the testers"),
                    "descr": _("Thanks! (testing, advices, bugs)"),
                    },
                {
                    "author": _("Contribute!"),
                    "descr": _("Join us on https://capbattleship.tuxfamily.org/"),
                    },
                ]

        font_descr = pygame.font.Font(FONT, 18)
        font_name = pygame.font.Font(FONT, 16)
        font_name.set_bold(False)
        font_url = pygame.font.Font(FONT, 16)
        font_url.set_italic(True)
        font_licence = pygame.font.Font(FONT, 16)

        topleft = (400, 40)
        for q in self.quotes:
            descr = font_descr.render(q['descr'], 1, WHITE)
            descr_r = descr.get_rect()
            descr_r.topleft = topleft

            name = font_name.render(q['author'], 1, ORANGE)
            name_r = name.get_rect()
            (x, y) = descr_r.bottomleft
            name_r.topleft = (x, y+5)

            url = font_name.render(q.get('url',' '), 1, WHITE)
            url_r = url.get_rect()
            (x, y) = name_r.bottomright
            url_r.bottomleft = (x+15, y)

            licence = font_licence.render(q.get('licence', ' '), 1, ORANGE)
            licence_r = licence.get_rect()
            (x, y) = url_r.bottomright
            licence_r.bottomleft = (x+15, y)

            self.bg.blit(descr, descr_r)
            self.bg.blit(name,  name_r)
            self.bg.blit(url,  url_r)
            self.bg.blit(licence,  licence_r)

            (x, y) = name_r.bottomleft
            topleft = (x, y+15)


        # Precompute menu
        self.menu_entries = []

        self.menu_entries.append(
                (_("Menu"), 'm', Stage.MENU, {})
                )

        ## Reference point for menu
        menu_center = (WIDTH//2, int(HEIGHT*0.9))

        ## Font face/size for menu entries
        menu_font = pygame.font.Font(FONT, 20)

        ## Generate menu's sprites
        self.menu_sprites = []
        for n,(text, key, stage, opt) in enumerate(self.menu_entries):
            active   = menu_font.render(text, 1, ORANGE)
            inactive = menu_font.render(text, 1, WHITE)

            rect = active.get_rect()
            rect.center = menu_center

            self.menu_sprites.append((rect, inactive, active))

        # Initially select first entry
        self.menu_index = 0

    def update(self):
        pass

    def draw(self, s):
        update_rects = []

        # Draw background
        s.blit(self.bg, (0, 0))

        # Draw menu
        for (n, (rect, inactive, active)) in enumerate(self.menu_sprites):
            if self.menu_index == n:
                item = active
            else:
                item = inactive
            s.blit(item, rect)
            update_rects.append(rect)

        return update_rects

    def input(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            for n, ((rect,inactive,active), (text,key,stage,opt)) in enumerate(zip(self.menu_sprites, self.menu_entries)):
                if rect.collidepoint(event.pos):
                    self.rnd.state.options['choice'] = stage
                    self.rnd.state.options['opt'] = opt
                    self.rnd.state.options['needs_redraw'] = True
        elif event.type == pygame.KEYDOWN:
            for n, (text,key,stage,opt) in enumerate(self.menu_entries):
                if event.unicode == key:
                    self.rnd.state.options['choice'] = stage
                    self.rnd.state.options['opt'] = opt
                    self.rnd.state.options['needs_redraw'] = True
            if event.key == pygame.K_DOWN:
                self.menu_index += 1
                self.menu_index %= len(self.menu_entries)
                self.rnd.state.options['needs_redraw'] = True
            elif event.key == pygame.K_UP:
                self.menu_index += len(self.menu_entries)-1
                self.menu_index %= len(self.menu_entries)
                self.rnd.state.options['needs_redraw'] = True
            if event.key == pygame.K_RETURN:
                if self.menu_index is not None:
                    self.rnd.state.options['choice'] = self.menu_entries[self.menu_index][2]
                    self.rnd.state.options['opt'] = self.menu_entries[self.menu_index][3]
                    self.rnd.state.options['needs_redraw'] = True
            elif event.key == pygame.K_ESCAPE:
                    self.rnd.state.options['choice'] = Stage.MENU
                    self.rnd.state.options['choice'] = {}
                    self.rnd.state.options['needs_redraw'] = True
        elif event.type == pygame.MOUSEMOTION:
            for n, ((rect,inactive,active), (text,key,stage,opt)) in enumerate(zip(self.menu_sprites, self.menu_entries)):
                if rect.collidepoint(event.pos):
                    self.menu_index = n
                    self.rnd.state.options['needs_redraw'] = True

class GuiSelection:
    def __init__(self, rnd):
        self.rnd = rnd

        # Precompute background
        self.bg = pygame.Surface((WIDTH, HEIGHT))
        self.bg.fill(DARKGRAY)
        pygame.draw.line(self.bg, WHITE, (WIDTH//2, int(HEIGHT*0.05)), (WIDTH//2, int(HEIGHT*0.80)))

        # Some alpha magic needed because convert alpha breaks set_alpha
        skullhead_center = (WIDTH//4, HEIGHT//4)
        skullhead_size = int(WIDTH*0.3)
        skullhead_raw = pygame.image.load("gfx/skullhead.png").convert_alpha()
        skullhead_raw = pygame.transform.smoothscale(skullhead_raw, (skullhead_size, skullhead_size))
        skullhead = pygame.Surface(skullhead_raw.get_size()).convert()
        skullhead.fill(DARKGRAY)
        skullhead.blit(skullhead_raw, (0, 0))
        skullhead.set_alpha(80)

        skullhead_rect = skullhead.get_rect()
        skullhead_rect.center = skullhead_center

        self.bg.blit(skullhead, skullhead_rect)

        # Precompute menu
        self.menu_next = pygame.image.load("gfx/button_next.png").convert_alpha()
        self.menu_next = pygame.transform.smoothscale(self.menu_next, (50, 50))
        self.menu_next_hover = pygame.image.load("gfx/button_next_hover.png").convert_alpha()
        self.menu_next_active = False

        self.menu_prev= pygame.transform.flip(self.menu_next, True, False)
        self.menu_prev_hover = pygame.transform.flip(self.menu_next_hover, True, False)
        self.menu_prev_active = False

        self.menu_ok = pygame.image.load("gfx/button_ok.png").convert_alpha()
        self.menu_ok = pygame.transform.smoothscale(self.menu_ok, (100, 100))
        self.menu_ok_hover = pygame.image.load("gfx/button_ok_hover.png").convert_alpha()
        self.menu_ok_hover = pygame.transform.smoothscale(self.menu_ok_hover, (100, 100))
        self.menu_ok_active = False

        self.menu_ok_rect = self.menu_ok.get_rect()
        self.menu_ok_rect.midbottom = (self.bg.get_width()//2, int(HEIGHT * 0.95))

        menu_font = pygame.font.Font(FONT, 30)
        self.menu_label = menu_font.render(_("Choose your pirate"), 1, WHITE)

        self.menu_label_rect = self.menu_label.get_rect()
        self.menu_label_rect.midtop = (self.bg.get_width()//4, int(HEIGHT * 0.52))
        self.menu_next_rect = self.menu_next.get_rect()
        x,y = self.menu_label_rect.midright
        self.menu_next_rect.midleft = x+50, y
        self.menu_prev_rect = self.menu_prev.get_rect()
        x,y = self.menu_label_rect.midleft
        self.menu_prev_rect.midright = x-50, y

        characters_descr = [
                "On sait peu de choses d'Ethel Bonny, dont seuls quelques récits de pirates font mention.  Elle serait liée à Anne Bonny, peut-être une descendante spirituelle de cette dernière. Une chose est sûre, c'est une commandante hors pair et aucun pirate des sept mers ne lui chercherait des noises sans s'être bien préparé.",
                "Bill Wright est un corsaire originaire de Saint Domingue. Acceptant une commission de guerre du gouverneur français, il fit plus tard une descente dans la colonie espagnole de Ségovie (aujourd'hui le Nicaragua) avec plusieurs autres corsaires.",
                "Les matelots navigant dans les mers des Caraïbes connaissent tous cet écossais à la barbe noire insolite. Greaves a l'humour aussi tranchant que ses deux sabres. Ont dit que ses adversaires deviennent fou, ou perdent la tête.",
                #"Damien Monbars, dit Monbars l'exterminateur, est un pirate Français du XVIIe siècle qui attaquait les bateaux espagnols afin de venger le peuple indien et les esclaves noirs.",
                ]

        charmenu_center = (int(WIDTH * 0.25), int(HEIGHT * 0.65))
        portrait_center = (int(WIDTH * 0.75), int(HEIGHT * 0.5))
        portrait_size = int(WIDTH * 0.5)

        charname_font = pygame.font.Font(FONT, 40)
        chardescr_font = pygame.font.Font(FONT, 18)
        descr_width = 50

        self.charmenu_items = []
        cmaxwidth,cmaxheight=0,0
        dmaxwidth,dmaxheight=0,0
        for n,((name, slug), descr) in enumerate(zip(self.rnd.characters,characters_descr)):
            charname = charname_font.render(name, 1, WHITE)
            crect = charname.get_rect()
            crect.center = charmenu_center

            chardescr = [ chardescr_font.render(line, 1, WHITE) for line in cut_text(descr_width, descr)]

            portrait = pygame.image.load("gfx/portrait_"+slug+".png")
            portrait = pygame.transform.smoothscale(portrait, (portrait_size, portrait_size))
            prect = portrait.get_rect()
            prect.center = portrait_center
            self.charmenu_items.append((n, charname, crect, portrait, prect, chardescr))
            if crect.width > cmaxwidth:
                cmaxwidth=crect.width
            if crect.height > cmaxheight:
                cmaxheight=crect.height

            s = sum([line.get_height() for line in chardescr])
            if s > dmaxheight:
                dmaxheight=s
            m = max([line.get_width() for line in chardescr])
            if m > dmaxwidth:
                dmaxwidth=m

        self.charmenu_crect = pygame.Rect((0, 0), (cmaxwidth, cmaxheight))
        self.charmenu_crect.center = charmenu_center

        self.charmenu_drect = pygame.Rect(0, 0, dmaxwidth, dmaxheight)
        self.charmenu_drect.midtop = self.charmenu_crect.midbottom
        self.charmenu_drect.top += 15

        self.charmenu_index = 0

    def update(self):
        pass

    def draw(self, s):
        update_rects = []

        # Draw background
        s.blit(self.bg, (0, 0))

        for (n, charname, crect, portrait, prect, chardescr) in self.charmenu_items:
            if self.charmenu_index == n:
                s.blit(charname, crect)
                x,y = crect.midbottom
                midbottom = self.charmenu_drect.midtop
                for line in chardescr:
                    rect = line.get_rect()
                    rect.midtop = midbottom
                    midbottom = rect.midbottom
                    s.blit(line, rect)
                s.blit(portrait, prect)
                update_rects.append(prect)
        update_rects.append(self.charmenu_crect)
        update_rects.append(self.charmenu_drect)

        s.blit(self.menu_label, self.menu_label_rect)

        if self.menu_next_active:
            s.blit(self.menu_next_hover, self.menu_next_rect)
        else:
            s.blit(self.menu_next, self.menu_next_rect)
        update_rects.append(self.menu_next_rect)

        if self.menu_prev_active:
            s.blit(self.menu_prev_hover, self.menu_prev_rect)
        else:
            s.blit(self.menu_prev, self.menu_prev_rect)
        update_rects.append(self.menu_prev_rect)

        if self.menu_ok_active:
            s.blit(self.menu_ok_hover, self.menu_ok_rect)
        else:
            s.blit(self.menu_ok, self.menu_ok_rect)
        update_rects.append(self.menu_ok_rect)

        return update_rects

    def input(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.menu_next_rect.collidepoint(event.pos):
                self.charmenu_index += 1
                self.charmenu_index %= len(self.charmenu_items)
                self.rnd.state.options['needs_redraw'] = True
            elif self.menu_prev_rect.collidepoint(event.pos):
                self.charmenu_index += len(self.charmenu_items) - 1
                self.charmenu_index %= len(self.charmenu_items)
                self.rnd.state.options['needs_redraw'] = True
            elif self.menu_ok_rect.collidepoint(event.pos):
                self.rnd.state.options['choice'] = self.charmenu_index
                self.rnd.state.options['needs_redraw'] = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                self.charmenu_index += 1
                self.charmenu_index %= len(self.charmenu_items)
                self.rnd.state.options['needs_redraw'] = True
            elif event.key == pygame.K_LEFT:
                self.charmenu_index += len(self.charmenu_items) - 1
                self.charmenu_index %= len(self.charmenu_items)
                self.rnd.state.options['needs_redraw'] = True
            elif event.key == pygame.K_RETURN:
                self.rnd.state.options['choice'] = self.charmenu_index
            elif event.key == pygame.K_ESCAPE:
                self.rnd.state.switch(Stage.MENU)
                self.rnd.state.options['needs_redraw'] = True
        elif event.type == pygame.MOUSEMOTION:
            if self.menu_next_rect.collidepoint(event.pos):
                if not self.menu_next_active:
                    self.menu_next_active = True
                    self.rnd.state.options['needs_redraw'] = True
            elif self.menu_prev_rect.collidepoint(event.pos):
                if not self.menu_prev_active:
                    self.menu_prev_active = True
                    self.rnd.state.options['needs_redraw'] = True
            elif self.menu_ok_rect.collidepoint(event.pos):
                if not self.menu_ok_active:
                    self.menu_ok_active = True
                    self.rnd.state.options['needs_redraw'] = True
            else:
                if self.menu_next_active or self.menu_prev_active or self.menu_ok_active:
                    self.menu_next_active = False
                    self.menu_prev_active = False
                    self.menu_ok_active = False
                    self.rnd.state.options['needs_redraw'] = True

class GuiSetupShips:
    def coords_to_pos(self, coords, topleft):
        y,x = coords
        a = self.cell_tr[0]
        b = self.cell_tl[0]
        c = self.cell_tr[1]
        d = -self.cell_tl[1]

        bx = x*a + y*b
        by = x*c + y*d

        by += 10*self.cell_tl[1]

        # ocean offset
        bx += topleft[0]
        by += topleft[1]
        return (int(bx), int(by))

    def pos_to_coords(self, pos, topleft):
        bx, by = pos
        # ocean offset
        bx -= topleft[0]
        by -= topleft[1]

        by -= 10*self.cell_tl[1]

        a = -self.cell_tl[1]
        b = -self.cell_tl[0]
        c = -self.cell_tr[1]
        d =  self.cell_tr[0]

        det = a*d-b*c
        x, y = (a*bx + b*by)/det, (c*bx + d*by)/det
        x, y = math.floor(x), math.floor(y)

        return (y,x)

    def __init__(self, rnd):
        self.rnd = rnd

        self.show_help = False
        self.tmp_ship = None
        self.hlp_boat_num = None
        self.tmp_ship = Ship(size=2, x=4, y=4, direction=Direction.SOUTH)
        self.debug = False
        self.just_toggled=False

        # Precompute background
        self.bg = pygame.Surface((WIDTH,HEIGHT))
        self.bg.fill(BLUEGRAY)

        ## Render player portrait
        pygame.draw.circle(self.bg, LIGHTBLUEGRAY, (int(WIDTH*0.05), int(HEIGHT*0.15)), int(WIDTH*0.13))
        portrait_size = 240

        name,slug = self.rnd.characters[self.rnd.player.character]
        portrait_player = pygame.image.load("gfx/portrait_"+slug+".png")
        portrait_player = pygame.transform.smoothscale(portrait_player, (portrait_size, portrait_size))
        portrait_player_rect = portrait_player.get_rect()
        portrait_player_rect.center = (int(WIDTH*0.07), int(HEIGHT*0.18))
        self.bg.blit(portrait_player, portrait_player_rect)

        font_player = pygame.font.Font(FONT, 22)
        label_player = font_player.render(name, 1, WHITE)
        self.bg.blit(label_player, (250, 10))

        # Render grid
        self._prepare_grid(self.bg)

        # Precompute help pane
        help_pane = pygame.Surface((WIDTH,HEIGHT)).convert_alpha()
        help_pane.fill((0, 0, 0, 0))
        self._prepare_help_pane(help_pane)
        self.help_pane = help_pane

        hlp_button_active   = pygame.image.load("gfx/help_button_focus.png").convert_alpha()
        hlp_button_inactive = pygame.image.load("gfx/help_button.png").convert_alpha()

        self.hlp_button_topright = (WIDTH-10, 10)

        # Precompute help menu
        rect = hlp_button_active.get_rect()
        rect.topright = self.hlp_button_topright
        self.help_sprite = (rect, hlp_button_inactive, hlp_button_active)
        self.help_entry  = (None, 'h', None)


        # Preload ships
        self._prepare_ship_descr()


    def _prepare_help_pane(self, s):
        hlp_move =   pygame.image.load("gfx/help_move-boat.png").convert_alpha()
        hlp_rotate = pygame.image.load("gfx/help_rotate-boat.png").convert_alpha()
        hlp_ready =  pygame.image.load("gfx/help_ready-boat.png").convert_alpha()

        pygame.draw.line(s, WHITE, (WIDTH//2, int(HEIGHT*0.1)), (WIDTH//2, int(HEIGHT*0.3)))

        hlp_move_rect = hlp_move.get_rect()
        hlp_move_rect.bottomright = (int(WIDTH*0.75), int(HEIGHT*0.5))
        s.blit(hlp_move,   hlp_move_rect)

        hlp_rotate_rect = hlp_rotate.get_rect()
        hlp_rotate_rect.bottomleft = (int(WIDTH*0.75), int(HEIGHT*0.5))
        s.blit(hlp_rotate, hlp_rotate_rect)

        hlp_ready_rect = hlp_rotate.get_rect()
        hlp_ready_rect.midtop = (int(WIDTH*0.75), int(HEIGHT*0.5))
        s.blit(hlp_ready,  hlp_ready_rect)


    def _prepare_ship_descr(self):
        shipname_font = pygame.font.Font(FONT, 50)
        shipdescr_font = pygame.font.Font(FONT, 18)
        descr_width = 50
        self.ships_descr = [None, None]
        for name, size, descr in [
                (_("The Anger"),         2, _("Every shipman is only grousing day and night, which drives the captain completely mad. There is one thing you can do to help him unwind, that is be an easy pray. I would advise you not to…")),
                (_("The Victory Saber"), 3, _("They were twin brothers, incredibly talentuous swashbucklers, and the fell for a mysterious lady. She said only the most fearsome pirate would be her match. Will you try their cruelty?")),
                (_("The Pirates Storm"), 4, _("This vessel is known for its sneaky attacks during storms. Manœuvrable and fast, no ship can escape her in the raging waters. If the weather is taking a turn for the worse, be sure that you're alone…")),
                (_("The Princess"),      5, _("Rather than the vessel, 'The Princess' refers to its heartless captain. Famous buccaneer, she is said to be as rich as a king. Well, if she meets you, maybe she well become even richer…")),
                ]:
            ship = {}
            ship['name']        = name
            ship['rname']       = shipname_font.render(name, 1, WHITE)
            ship['descr']       = descr
            ship['rdescr']      = [ shipdescr_font.render(line, 1, WHITE) for line in cut_text(descr_width, descr)]
            ship['turntable']   = ShipTurntable(ship_size=size, pos=(int(WIDTH*0.75), int(HEIGHT*0.4)), loop=True)
            models = {}
            for direction,letter in [(Direction.NORTH, "N"), (Direction.SOUTH, "S"), (Direction.EAST, "E"), (Direction.WEST, "W")]:
                view = pygame.image.load("gfx/boat_"+str(size)+"_"+letter+".png").convert_alpha()
                #view = pygame.transform.smoothscale(view, (int(5*self.cell_tl[0]),int(5*self.cell_tl[0])))
                models[direction] = view
            ship['models'] = models
            self.ships_descr.append(ship)

        self.ships_descr[2]['offset'] = {
                Direction.NORTH: ( 17,-17),
                Direction.SOUTH: ( 30, -7),
                Direction.EAST:  ( 36,-23),
                Direction.WEST:  ( 10,-14),
                }
        self.ships_descr[3]['offset'] = {
                Direction.NORTH: ( 10,-25),
                Direction.SOUTH: ( 41,  4),
                Direction.EAST:  ( 47,-27),
                Direction.WEST:  ( -5, -5),
                }
        self.ships_descr[4]['offset'] = {
                Direction.NORTH: ( -2,-36),
                Direction.SOUTH: ( 53, 16),
                Direction.EAST:  ( 69,-36),
                Direction.WEST:  (-16,  1),
                }
        self.ships_descr[5]['offset'] = {
                Direction.NORTH: ( -9, -45),
                Direction.SOUTH: ( 66,  32),
                Direction.EAST:  ( 81, -44),
                Direction.WEST:  (-36,  11),
                }
        name_max_w = max([ s['rname'].get_width() for s in self.ships_descr[2:]])
        descr_max_w = max([ max([l.get_width() for l in s['rdescr']]) for s in self.ships_descr[2:]])

        self.ship_details_rect = pygame.Rect((0,0), (max(name_max_w,descr_max_w),1))
        self.ship_details_rect.midtop = self.ships_descr[2]['turntable'].rect.midbottom
        self.ship_details_rect.top -= 50
        self.ship_details_rect.height = HEIGHT - self.ship_details_rect.top - 10

    def _prepare_grid(self, s):
        ocean_player = pygame.image.load("gfx/ocean_player.png").convert_alpha()

        ocean_width = 480
        top_center_x=int(510*480/870)
        left_center_y=int(267*480/870)
        ocean_ratio  = ocean_player.get_width()/ocean_player.get_height()
        ocean_zoom   = ocean_width / ocean_player.get_width()
        #ocean_player = pygame.transform.smoothscale(ocean_player, (ocean_width, int(ocean_width/ocean_ratio)))

        ocean_size   = ocean_player.get_size()

        # top left border of a grid cell
        self.cell_tl      = top_center_x/10 * ocean_zoom, left_center_y/10 * ocean_zoom
        # top right border of a grid cell
        self.cell_tr      = ocean_size[0]/10 - self.cell_tl[0], ocean_size[1]/10 - self.cell_tl[1]
        self.cell_w       = self.cell_tl[0] + self.cell_tr[0]
        self.cell_h       = self.cell_tl[1] + self.cell_tr[1]
        cell_angle = -math.asin((self.cell_tl[0]-self.cell_w/2)/self.cell_w) / math.pi * 180 + 45

        self.ocean_player_rect = ocean_player.get_rect()
        self.ocean_player_rect.center = (1*WIDTH//4, HEIGHT//2)
        s.blit(ocean_player, self.ocean_player_rect)

        corner = (self.ocean_player_rect.left, self.ocean_player_rect.top)

        # Drawing grid and legend
        grid_font = pygame.font.Font(FONT, 19)
        labels_cols = []
        labels_rows = []
        for i in range(0, 10):
            labels_rows.append(grid_font.render(str(i+1), 1, WHITE))
            labels_cols.append(grid_font.render(chr(ord('A') + i), 1, WHITE))
        for l in range(0,10+1):
            pygame.draw.line(s, WHITE, self.coords_to_pos(( l, 0), corner), self.coords_to_pos((l, 11), corner), 1)
            pygame.draw.line(s, WHITE, self.coords_to_pos((-1, l), corner), self.coords_to_pos((10, l), corner), 1)
        for l in range(0, 10):
            cx,cy = self.coords_to_pos((l, 10), corner)
            crect = labels_cols[l].get_rect()
            crect.centerx = cx + self.cell_tr[0]
            crect.centery = cy + self.cell_tr[1]/4
            s.blit(pygame.transform.rotate(labels_cols[l], cell_angle), crect)

            rx,ry = self.coords_to_pos((-1, l), corner)
            rrect = labels_rows[l].get_rect()
            rrect.centerx = rx + self.cell_tl[0]*3/4 -3
            rrect.centery = ry + self.cell_tr[1]*0/4
            s.blit(pygame.transform.rotate(labels_rows[l], cell_angle), rrect)

    def _draw_ship(self, s, ship, board_topleft):
        cx, cy = self.coords_to_pos((ship.x, ship.y), board_topleft)
        ship_pos = self.ships_descr[ship.size]['models'][ship.direction].get_rect()
        ship_pos.centerx = cx + self.ships_descr[ship.size]['offset'][ship.direction][0]
        ship_pos.centery = cy + self.ships_descr[ship.size]['offset'][ship.direction][1]
        s.blit(self.ships_descr[ship.size]['models'][ship.direction], ship_pos)
        return [ship_pos]

    def _draw_ship_descr(self, s):
        if self.tmp_ship is None:
            return []

        update_rects=[]

        help_top = 300
        help_left = 550

        if self.tmp_ship is not None:
            ship = self.ships_descr[self.tmp_ship.size]
            ship['turntable'].draw(s)
            update_rects.append(ship['turntable'].rect)
            name_rect = ship['rname'].get_rect()
            name_rect.midtop = ship['turntable'].rect.midbottom
            name_rect.top -= 50
            s.blit(ship['rname'], name_rect)

            top = name_rect.top + 80
            for n,rline in enumerate(ship['rdescr']):
                rect = rline.get_rect()
                rect.midtop = name_rect.midbottom
                rect.top += 20 + n*25
                s.blit(rline, rect)
            update_rects.append(self.ship_details_rect)

        return update_rects

    def _draw_board_objects(self, s, board_topleft, board):
        tmp_drawn = False
        for ship in sorted(board.ships, key=lambda ship: ship.y):
            if self.tmp_ship is not None and not tmp_drawn and ship.y >= self.tmp_ship.y:
                self._draw_ship(s, self.tmp_ship, board_topleft)
                tmp_drawn = True
            self._draw_ship(s, ship, board_topleft)
        if self.tmp_ship is not None and not tmp_drawn:
            self._draw_ship(s, self.tmp_ship, board_topleft)

    def update(self):
        if self.rnd.state.options.get('direction', None) is None:
            self.rnd.state.options['direction'] = Direction.EAST
        if self.rnd.state.options.get('x', None) is None:
            self.rnd.state.options['x'] = 4
        if self.rnd.state.options.get('y', None) is None:
            self.rnd.state.options['y'] = 4

        if not self.rnd.state.options.get('auto', False) and len(self.rnd.player.ships) < len(self.rnd.ships) and (
                self.tmp_ship is None
                or self.tmp_ship.x != self.rnd.state.options['x']
                or self.tmp_ship.y != self.rnd.state.options['y']
                or self.tmp_ship.direction != self.rnd.state.options['direction']
                or self.tmp_ship.size != self.rnd.ships[len(self.rnd.player.ships)][1]
                ):
            tmp_ship_size = self.rnd.ships[len(self.rnd.player.ships)][1]
            self.tmp_ship = Ship(
                    self.rnd.state.options['x'],
                    self.rnd.state.options['y'],
                    self.rnd.state.options['direction'],
                    tmp_ship_size,
                    self.ships_descr[tmp_ship_size]['name']
                    )
            self.rnd.state.options['needs_redraw'] = True

        if not self.show_help and self.tmp_ship is not None and self.rnd.params.get('animations', True):
            self.ships_descr[self.tmp_ship.size]['turntable'].update()
            self.rnd.state.options['needs_redraw'] = True

    def draw(self, s):
        update_rects = []

        s.blit(self.bg, (0, 0))

        self._draw_board_objects(s, self.ocean_player_rect.topleft, self.rnd.player)

        bigocean = self.ocean_player_rect.copy()
        bigocean.height+=30
        bigocean.midbottom = self.ocean_player_rect.midbottom
        update_rects.append(bigocean)

        if not self.show_help:
            s.blit(self.help_sprite[2], self.help_sprite[0])
            update_rects+=self._draw_ship_descr(s)
        else:
            s.blit(self.help_pane, (0, 0))
            s.blit(self.help_sprite[1], self.help_sprite[0])
        update_rects.append(self.help_sprite[0])

        if self.just_toggled:
            self.just_toggled=False
            update_rects=[self.bg.get_rect()]

        return update_rects

    def input(self, event):
        # help button
        if event.type == pygame.MOUSEBUTTONDOWN and self.help_sprite[0].collidepoint(event.pos):
                self.show_help = not self.show_help
                self.just_toggled = True
                self.rnd.state.options['needs_redraw'] = True
        # help key
        elif event.type == pygame.KEYDOWN and event.unicode == self.help_entry[1]:
                self.show_help = not self.show_help
                self.just_toggled = True
                self.rnd.state.options['needs_redraw'] = True
        # boat mouse command and rotation
        elif event.type == pygame.MOUSEBUTTONDOWN and self.ocean_player_rect.collidepoint(event.pos):
            if event.button == 3:
                self.rnd.state.options['needs_redraw'] = True
                if self.rnd.state.options['direction'] == Direction.EAST:
                    self.rnd.state.options['direction'] = Direction.SOUTH
                elif self.rnd.state.options['direction'] == Direction.SOUTH:
                    self.rnd.state.options['direction'] = Direction.WEST
                elif self.rnd.state.options['direction'] == Direction.WEST:
                    self.rnd.state.options['direction'] = Direction.NORTH
                elif self.rnd.state.options['direction'] == Direction.NORTH:
                    self.rnd.state.options['direction'] = Direction.EAST
            elif event.button == 1:
                self.rnd.state.options['ready'] = True
                self.tmp_ship = None
            elif event.button == 2:
                self.rnd.state.options['auto'] = True
                self.tmp_ship = None
        # boat mouse position
        elif event.type == pygame.MOUSEMOTION and self.ocean_player_rect.collidepoint(event.pos):
                x,y = self.pos_to_coords(event.pos, self.ocean_player_rect.topleft)
                try:
                    tmp_ship_size = self.rnd.ships[len(self.rnd.player.ships)][1]
                    if x>= 0 and y >= 0 and x < self.rnd.width and y < self.rnd.width and (
                            self.rnd.state.options['direction'] == Direction.NORTH and y >= tmp_ship_size-1
                            or self.rnd.state.options['direction'] == Direction.SOUTH and y <= self.rnd.width - tmp_ship_size
                            or self.rnd.state.options['direction'] == Direction.EAST  and x <= self.rnd.width - tmp_ship_size
                            or self.rnd.state.options['direction'] == Direction.WEST  and x >= tmp_ship_size-1
                            ):
                        self.rnd.state.options['x'] = x
                        self.rnd.state.options['y'] = y
                except:
                    pass
        # boat keyboard command and position
        elif event.type == pygame.KEYDOWN:
            # rotation
            if event.unicode == 'r':
                if   self.rnd.state.options['direction'] == Direction.EAST:
                    self.rnd.state.options['direction']   = Direction.SOUTH
                elif self.rnd.state.options['direction'] == Direction.SOUTH:
                    self.rnd.state.options['direction']   = Direction.WEST
                elif self.rnd.state.options['direction'] == Direction.WEST:
                    self.rnd.state.options['direction']   = Direction.NORTH
                elif self.rnd.state.options['direction'] == Direction.NORTH:
                    self.rnd.state.options['direction']   = Direction.EAST
            # position and command
            elif event.key in [pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT, pygame.K_RETURN, pygame.K_SPACE] and not self.debug:
                if self.tmp_ship is not None and not self.rnd.state.options.get('ready', False):
                    self.rnd.state.options['needs_redraw'] = True
                    if event.key == pygame.K_UP:
                        if  self.tmp_ship.ymin > 0:
                            self.tmp_ship.y    -= 1
                            self.tmp_ship.ymin -= 1
                            self.tmp_ship.ymax -= 1
                            self.rnd.state.options['y'] = self.tmp_ship.y
                    elif event.key == pygame.K_DOWN:
                        if  self.tmp_ship.ymax < 9:
                            self.tmp_ship.y    += 1
                            self.tmp_ship.ymin += 1
                            self.tmp_ship.ymax += 1
                            self.rnd.state.options['y'] = self.tmp_ship.y
                    elif event.key == pygame.K_LEFT:
                        if  self.tmp_ship.xmin > 0:
                            self.tmp_ship.x    -= 1
                            self.tmp_ship.xmin -= 1
                            self.tmp_ship.xmax -= 1
                            self.rnd.state.options['x'] = self.tmp_ship.x
                    elif event.key == pygame.K_RIGHT:
                        if  self.tmp_ship.xmax < 9:
                            self.tmp_ship.x    += 1
                            self.tmp_ship.xmin += 1
                            self.tmp_ship.xmax += 1
                            self.rnd.state.options['x'] = self.tmp_ship.x
                    elif event.key == pygame.K_RETURN:
                        self.rnd.state.options['ready'] = True
                        self.tmp_ship = None
                    elif event.key == pygame.K_SPACE:
                        self.rnd.state.options['auto'] = True
                        self.tmp_ship = None
            elif event.key == pygame.K_ESCAPE:
                self.rnd.state.switch(Stage.MENU, save=True)
            # debug
            elif event.unicode == 'd':
                self.debug = not self.debug
            elif event.key in [pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT, pygame.K_RETURN] and self.debug:
                if self.tmp_ship is not None:
                    self.rnd.state.options['needs_redraw'] = True
                    ox,oy = self.ships_descr[self.tmp_ship.size]['offset'][self.tmp_ship.direction]
                    if event.key == pygame.K_UP:
                        ox,oy = ox, oy-1
                    elif event.key == pygame.K_DOWN:
                        ox,oy = ox, oy+1
                    elif event.key == pygame.K_LEFT:
                        ox,oy = ox-1, oy
                    elif event.key == pygame.K_RIGHT:
                        ox,oy = ox+1, oy
                    elif event.key == pygame.K_RETURN:
                        print(self.ships_descr[self.tmp_ship.size]['offset'])
                    self.ships_descr[self.tmp_ship.size]['offset'][self.tmp_ship.direction] = ox,oy

class GuiBattleShips:
    def coords_to_pos(self, coords, topleft):
        y,x = coords
        a = self.cell_tr[0]
        b = self.cell_tl[0]
        c = self.cell_tr[1]
        d = -self.cell_tl[1]

        bx = x*a + y*b
        by = x*c + y*d

        by += 10*self.cell_tl[1]

        # ocean offset
        bx += topleft[0]
        by += topleft[1]
        return (int(bx), int(by))

    def pos_to_coords(self, pos, topleft):
        bx, by = pos
        # ocean offset
        bx -= topleft[0]
        by -= topleft[1]

        by -= 10*self.cell_tl[1]

        a = -self.cell_tl[1]
        b = -self.cell_tl[0]
        c = -self.cell_tr[1]
        d =  self.cell_tr[0]

        det = a*d-b*c
        x, y = (a*bx + b*by)/det, (c*bx + d*by)/det
        x, y = math.floor(x), math.floor(y)

        return (y,x)

    def __init__(self, rnd):
        self.rnd = rnd

        self.event_anims  = []
        self.prev_player_hits = self.rnd.player.hits
        self.prev_player_miss = self.rnd.player.miss
        self.prev_cpu_hits    = self.rnd.cpu.hits
        self.prev_cpu_miss    = self.rnd.cpu.miss

        self.debug = False
        self.draw_enemy_ships = False
        self.aiming = 4,4

        bg = pygame.Surface((WIDTH, HEIGHT))
        bg.fill(BLUEGRAY)

        pygame.draw.circle(bg, LIGHTBLUEGRAY, (int(WIDTH*0.05), int(HEIGHT*0.15)), int(WIDTH*0.13))
        pygame.draw.circle(bg, LIGHTBLUEGRAY, (int(WIDTH*0.95), int(HEIGHT*0.15)), int(WIDTH*0.13))
        self.portrait_player = None
        self.portrait_player_rect = None
        self.portrait_cpu = None
        self.portrait_cpu_rect = None

        self.font_player = pygame.font.Font(FONT, 22)
        self._prepare_grids(bg)

        skullhead_center = (
                WIDTH//2,
                HEIGHT//8,
                )
        skullhead_size = int(WIDTH*0.125)
        skullhead_raw = pygame.image.load("gfx/skullhead.png").convert_alpha()
        skullhead_raw = pygame.transform.smoothscale(skullhead_raw, (skullhead_size, skullhead_size))
        skullhead = pygame.Surface(skullhead_raw.get_size()).convert()
        skullhead.fill(BLUEGRAY)
        skullhead.blit(skullhead_raw, (0, 0))
        skullhead.set_alpha(80)
        skullhead_rect = skullhead.get_rect()
        skullhead_rect.center = skullhead_center

        pygame.draw.line(bg, WHITE, skullhead_rect.midbottom, (skullhead_rect.midbottom[0], skullhead_rect.midbottom[1]+int(WIDTH*0.1)))

        bg.blit(skullhead, skullhead_rect)

        self.bg = bg
        self.sfx = (pygame.mixer.Channel(0), pygame.mixer.Channel(1))

        self._prepare_ship_descr()

        self.miss = pygame.image.load("gfx/cross_white.png").convert_alpha()
        #self.miss = pygame.transform.smoothscale(self.miss, (int(self.cell_w/2), int(self.cell_h/2)))
        self.hit  = pygame.image.load("gfx/cross_orange.png").convert_alpha()
        #self.hit  = pygame.transform.smoothscale(self.hit, (int(self.cell_w/2), int(self.cell_h/2)))
        self.sunk = pygame.image.load("gfx/cross_red.png").convert_alpha()
        #self.sunk = pygame.transform.smoothscale(self.sunk, (int(self.cell_w/2), int(self.cell_h/2)))

        self.font_stats = pygame.font.Font(FONT, 16)

        self.labels_stats = {}
        self.labels_stats["shots"] = self.font_stats.render(_("Shots"), 1, WHITE)
        self.labels_stats["miss"]  = self.font_stats.render(_("Miss"), 1, WHITE)
        self.labels_stats["hit"]   = self.font_stats.render(_("Hit"), 1, WHITE)
        self.labels_stats["sunk"]  = self.font_stats.render(_("Sunk"), 1, WHITE)


    def _prepare_ship_descr(self):
        self.ships_descr = [None, None]
        for name, size, descr in [
                (_("The Anger"),         2, _("Every shipman is only grousing day and night, which drives the captain completely mad. There is one thing you can do to help him unwind, that is be an easy pray. I would advise you not to…")),
                (_("The Victory Saber"), 3, _("They were twin brothers, incredibly talentuous swashbucklers, and the fell for a mysterious lady. She said only the most fearsome pirate would be her match. Will you try their cruelty?")),
                (_("The Pirates Storm"), 4, _("This vessel is known for its sneaky attacks during storms. Manœuvrable and fast, no ship can escape her in the raging waters. If the weather is taking a turn for the worse, be sure that you're alone…")),
                (_("The Princess"),      5, _("Rather than the vessel, 'The Princess' refers to its heartless captain. Famous buccaneer, she is said to be as rich as a king. Well, if she meets you, maybe she well become even richer…")),
                ]:
            ship = {}
            ship['name']        = name
            ship['descr']       = descr
            models = {}
            for direction,letter in [(Direction.NORTH, "N"), (Direction.SOUTH, "S"), (Direction.EAST, "E"), (Direction.WEST, "W")]:
                view = pygame.image.load("gfx/boat_"+str(size)+"_"+letter+".png").convert_alpha()
                #view = pygame.transform.smoothscale(view, (int(5*self.cell_tl[0]),int(5*self.cell_tl[0])))
                models[direction] = view
            ship['models'] = models
            self.ships_descr.append(ship)

        self.ships_descr[2]['offset'] = {
                Direction.NORTH: ( 17,-17),
                Direction.SOUTH: ( 30, -7),
                Direction.EAST:  ( 36,-23),
                Direction.WEST:  ( 10,-14),
                'P': ( 0,  0),
                }
        self.ships_descr[3]['offset'] = {
                Direction.NORTH: ( 10,-25),
                Direction.SOUTH: ( 41,  4),
                Direction.EAST:  ( 47,-27),
                Direction.WEST:  ( -5, -5),
                'P': ( 0,  0),
                }
        self.ships_descr[4]['offset'] = {
                Direction.NORTH: ( -2,-36),
                Direction.SOUTH: ( 53, 16),
                Direction.EAST:  ( 69,-36),
                Direction.WEST:  (-16,  1),
                'P': ( 0,  0),
                }
        self.ships_descr[5]['offset'] = {
                Direction.NORTH: ( -9, -45),
                Direction.SOUTH: ( 66,  32),
                Direction.EAST:  ( 81, -44),
                Direction.WEST:  (-36,  11),
                'P': ( 0,  0),
                }

    def _prepare_grids(self, s):
        ocean_player = pygame.image.load("gfx/ocean_player.png").convert_alpha()
        ocean_cpu    = pygame.image.load("gfx/ocean_cpu.png").convert_alpha()

        ocean_width = 480
        top_center_x=int(510*480/870)
        left_center_y=int(267*480/870)
        ocean_ratio  = ocean_player.get_width()/ocean_player.get_height()
        ocean_zoom   = ocean_width / ocean_player.get_width()
        #ocean_player = pygame.transform.smoothscale(ocean_player, (ocean_width, int(ocean_width/ocean_ratio)))
        #ocean_cpu    = pygame.transform.smoothscale(ocean_cpu,    (ocean_width, int(ocean_width/ocean_ratio)))

        ocean_size   = ocean_player.get_size()

        # top left border of a grid cell
        self.cell_tl      = top_center_x/10 * ocean_zoom, left_center_y/10 * ocean_zoom
        # top right border of a grid cell
        self.cell_tr      = ocean_size[0]/10 - self.cell_tl[0], ocean_size[1]/10 - self.cell_tl[1]
        self.cell_w       = self.cell_tl[0] + self.cell_tr[0]
        self.cell_h       = self.cell_tl[1] + self.cell_tr[1]
        cell_angle = -math.asin((self.cell_tl[0]-self.cell_w/2)/self.cell_w) / math.pi * 180 + 45

        self.ocean_player_rect = ocean_player.get_rect()
        self.ocean_cpu_rect    = ocean_cpu.get_rect()
        self.ocean_player_rect.center = (1*WIDTH//4, HEIGHT//2)
        self.ocean_cpu_rect.center    = (3*WIDTH//4, HEIGHT//2)
        s.blit(ocean_player, self.ocean_player_rect)
        s.blit(ocean_cpu, self.ocean_cpu_rect)

        for corner in [self.ocean_player_rect.topleft, self.ocean_cpu_rect.topleft]:
            # Drawing grid and legend
            grid_font = pygame.font.Font(FONT, 19)
            labels_cols = []
            labels_rows = []
            for i in range(0, 10):
                labels_rows.append(grid_font.render(str(i+1), 1, WHITE))
                labels_cols.append(grid_font.render(chr(ord('A') + i), 1, WHITE))
            for l in range(0,10+1):
                pygame.draw.line(s, WHITE, self.coords_to_pos(( l, 0), corner), self.coords_to_pos((l, 11), corner), 1)
                pygame.draw.line(s, WHITE, self.coords_to_pos((-1, l), corner), self.coords_to_pos((10, l), corner), 1)
            for l in range(0, 10):
                cx,cy = self.coords_to_pos((l, 10), corner)
                crect = labels_cols[l].get_rect()
                crect.centerx = cx + self.cell_tr[0]
                crect.centery = cy + self.cell_tr[1]/4
                s.blit(pygame.transform.rotate(labels_cols[l], cell_angle), crect)

                rx,ry = self.coords_to_pos((-1, l), corner)
                rrect = labels_rows[l].get_rect()
                rrect.centerx = rx + self.cell_tl[0]*3/4 -3
                rrect.centery = ry + self.cell_tr[1]*0/4
                s.blit(pygame.transform.rotate(labels_rows[l], cell_angle), rrect)

    def _draw_stats(self, s, board, other):
        update_rects = []
        left = board.is_player
        h = 24
        H = h+5

        if left:
            x,y = 20,HEIGHT-20
        else:
            x,y = WIDTH-20,HEIGHT-20

        counts = [len(other.sunk), len(other.hits), len(other.miss), len(other.miss) + len(other.hits)]
        labels_counts = [self.font_stats.render(str(c), 1, BLACK) for c in counts]
        counts_width = self.font_stats.render("100", 1, BLACK).get_width()

        for n,(lbl, icon) in enumerate([("sunk", self.sunk), ("hit", self.hit), ("miss", self.miss), ("shots", None)]):
            icon_rect = self.miss.get_rect()
            ry = y-n*(icon_rect.height+10)
            if left:
                icon_rect.bottomleft = x,ry
            else:
                icon_rect.bottomright = x,ry
            if icon is not None:
                s.blit(icon, icon_rect)

            count_rect = labels_counts[n].get_rect()
            count_box = pygame.Rect((0,0), (counts_width+4, count_rect.height+4))
            if left:
                count_box.midleft = icon_rect.midright
                count_box.left += 10
            else:
                count_box.midright = icon_rect.midleft
                count_box.right -= 10
            count_rect.center = count_box.center
            pygame.draw.rect(s, WHITE, count_box)
            s.blit(labels_counts[n], count_rect)

            label_rect = self.labels_stats[lbl].get_rect()
            if left:
                label_rect.midleft = count_box.midright
                label_rect.left += 10
            else:
                label_rect.midright = count_box.midleft
                label_rect.right -= 10
            s.blit(self.labels_stats[lbl],  label_rect)

            update_rects.append(count_box)

        return update_rects



    def _draw_ship(self, s, ship, board_topleft):
        cx, cy = self.coords_to_pos((ship.x, ship.y), board_topleft)
        ship_pos = self.ships_descr[ship.size]['models'][ship.direction].get_rect()
        ship_pos.centerx = cx + self.ships_descr[ship.size]['offset'][ship.direction][0]
        ship_pos.centery = cy + self.ships_descr[ship.size]['offset'][ship.direction][1]
        s.blit(self.ships_descr[ship.size]['models'][ship.direction], ship_pos)

    def _draw_board_objects(self, s, board_topleft, board, draw_ships=True):
        update_rects = []
        # Drawing ploufs
        for (mx, my) in board.miss:
            a = [e[0] for e in self.event_anims if e[1]==(mx, my) and e[2]==board_topleft]
            if len(a) == 0:
                x, y = self.coords_to_pos((mx, my),board_topleft)
                cross_pos = self.miss.get_rect()
                cross_pos.center = (x + (self.cell_tl[0] + self.cell_tr[0])/2, y + (self.cell_tr[1]-self.cell_tl[1])/2)
                s.blit(self.miss, cross_pos)
                update_rects+=cross_pos
            else:
                if a[0].index is not None:
                    rl = a[0].draw(s)
                    update_rects += rl
        # Drawing ships
        if draw_ships:
            for ship in sorted(board.ships, key=lambda ship: ship.y):
                self._draw_ship(s, ship, board_topleft)

        # Sunk positions
        sunk_coords = []
        for ship in board.ships:
            if ship.get_health() == 0:
                for i in range(ship.xmin, ship.xmax+1):
                    for j in range(ship.ymin, ship.ymax+1):
                        sunk_coords.append((i,j))

        # Drawing boums
        for (hx, hy) in board.hits:
            a = [e[0] for e in self.event_anims if e[1]==(hx, hy) and e[2]==board_topleft]
            if len(a) == 0:
                x, y = self.coords_to_pos((hx, hy), board_topleft)
                cross_pos = self.hit.get_rect()
                cross_pos.center = (x + (self.cell_tl[0] + self.cell_tr[0])/2, y + (self.cell_tr[1]-self.cell_tl[1])/2)
                if (hx, hy) in sunk_coords:
                    s.blit(self.sunk, cross_pos)
                else:
                    s.blit(self.hit, cross_pos)
                update_rects+=cross_pos
            else:
                if a[0].index is not None:
                    rl = a[0].draw(s)
                    update_rects += rl

        return update_rects

    def update(self):
        if self.rnd.state.options.get('x', None) is None:
            self.rnd.state.options['x'] = self.aiming[0]
        if self.rnd.state.options.get('y', None) is None:
            self.rnd.state.options['y'] = self.aiming[1]
        self.aiming = (self.rnd.state.options['x'], self.rnd.state.options['y'])

        for (old, new, topleft, channel) in [
                (self.prev_player_hits, self.rnd.player.hits, self.ocean_player_rect.topleft, 0),
                (self.prev_cpu_hits,    self.rnd.cpu.hits,    self.ocean_cpu_rect.topleft, 1),
                ]:
            for (hx, hy) in new[len(old):]:
                (bx, by) = self.coords_to_pos((hx, hy), topleft)
                if self.rnd.params.get('animations', True):
                    self.event_anims.append((
                        #FxHit(pos=(bx, by), loop=False, resize_width=int(self.cell_w*2), offset=(self.cell_tl[0], -self.cell_tl[1] - 40)),
                        FxHit(pos=(bx, by), loop=False, offset=(self.cell_tl[0], -self.cell_tl[1] - 40)),
                        (hx, hy),
                        topleft
                        ))
                if self.rnd.params.get('sfx', True):
                    sound_file = choice(glob("sfx/hit-??.ogg"))
                    self.sfx[channel].queue(pygame.mixer.Sound(sound_file))

        self.prev_player_hits = self.rnd.player.hits.copy()
        self.prev_cpu_hits    = self.rnd.cpu.hits.copy()

        if self.portrait_player is None or self.portrait_cpu is None:
            pname,pslug = self.rnd.characters[self.rnd.player.character]
            cname,cslug = self.rnd.characters[self.rnd.cpu.character]
            portrait_size = 240
            self.portrait_player = pygame.image.load("gfx/portrait_"+pslug+".png").convert_alpha()
            self.portrait_cpu = pygame.image.load("gfx/portrait_"+cslug+".png").convert_alpha()
            self.portrait_player = pygame.transform.smoothscale(self.portrait_player, (portrait_size, portrait_size))
            self.portrait_cpu = pygame.transform.smoothscale(self.portrait_cpu, (portrait_size, portrait_size))
            self.portrait_cpu = pygame.transform.flip(self.portrait_cpu, True, False)
            self.portrait_player_rect = self.portrait_player.get_rect()
            self.portrait_cpu_rect = self.portrait_cpu.get_rect()
            self.portrait_player_rect.center = (int(WIDTH*0.07), int(HEIGHT*0.18))
            self.portrait_cpu_rect.center = (int(WIDTH*0.93), int(HEIGHT*0.18))

        for (old, new, topleft, channel) in [
                (self.prev_player_miss, self.rnd.player.miss, self.ocean_player_rect.topleft, 0),
                (self.prev_cpu_miss,    self.rnd.cpu.miss,    self.ocean_cpu_rect.topleft, 1),
                ]:
            for (hx, hy) in new[len(old):]:
                (bx, by) = self.coords_to_pos((hx, hy), topleft)
                if self.rnd.params.get('animations', True):
                    self.event_anims.append((
                        #FxMiss(pos=(bx, by), loop=False, resize_width=int(self.cell_w*1), offset=(self.cell_tl[0], 0)),
                        FxMiss(pos=(bx, by), loop=False, offset=(self.cell_tl[0]-3, +3)),
                        (hx, hy),
                        topleft
                        ))
                if self.rnd.params.get('sfx', True):
                    sound_file = choice(glob("sfx/miss-??.ogg"))
                    self.sfx[channel].queue(pygame.mixer.Sound(sound_file))

        self.prev_player_miss = self.rnd.player.miss.copy()
        self.prev_cpu_miss    = self.rnd.cpu.miss.copy()

        for (a, pos, topleft) in self.event_anims:
            a.update()

        if len(self.event_anims) > 0:
            self.rnd.state.options['needs_redraw'] = True

        self.event_anims = [ (a, (x,y), topleft) for (a, (x,y), topleft) in self.event_anims if not a.finished]


    def draw(self, s):
        update_rects = []

        s.blit(self.bg, (0, 0))

        s.blit(self.portrait_player, self.portrait_player_rect)
        s.blit(self.portrait_cpu, self.portrait_cpu_rect)

        for b in [self.ocean_player_rect, self.ocean_cpu_rect]:
            bigocean = b.copy()
            bigocean.height+=30
            bigocean.midbottom = b.midbottom
            update_rects.append(bigocean)

        label_player = self.font_player.render(self.rnd.characters[self.rnd.player.character][0], 1, WHITE)
        label_enemy  = self.font_player.render(self.rnd.characters[self.rnd.cpu.character][0], 1, WHITE)
        r = label_enemy.get_rect()
        r.topright = (s.get_width()-250, 10)
        s.blit(label_player, (250, 10))
        s.blit(label_enemy, r)

        x,y = self.aiming
        pygame.draw.polygon(s, ORANGE, [ 
            self.coords_to_pos((x, y),     self.ocean_cpu_rect.topleft),
            self.coords_to_pos((x+1, y),   self.ocean_cpu_rect.topleft),
            self.coords_to_pos((x+1, y+1), self.ocean_cpu_rect.topleft),
            self.coords_to_pos((x, y+1),   self.ocean_cpu_rect.topleft),
            ], 3)

        self._draw_board_objects(s, self.ocean_player_rect.topleft, self.rnd.player)
        self._draw_board_objects(s, self.ocean_cpu_rect.topleft, self.rnd.cpu, draw_ships=self.draw_enemy_ships)

        update_rects+=self._draw_stats(s, self.rnd.player, self.rnd.cpu)
        update_rects+=self._draw_stats(s, self.rnd.cpu, self.rnd.player)

        return update_rects


    def input(self, event):
            # mouse command
            if event.type == pygame.MOUSEBUTTONDOWN and self.ocean_cpu_rect.collidepoint(event.pos):
                if event.button == 1:
                    x,y = self.pos_to_coords(event.pos, self.ocean_cpu_rect.topleft)
                    if 0 <= x and 0 <= y and x < self.rnd.width and y < self.rnd.width:
                        self.rnd.state.options['needs_redraw'] = True
                        self.rnd.state.options['x'] = x
                        self.rnd.state.options['y'] = y
                        self.rnd.state.options['ready'] = True
                elif event.button == 2:
                    self.rnd.state.options['auto'] = True
                    self.rnd.state.options['needs_redraw'] = True
            # mouse position
            elif event.type == pygame.MOUSEMOTION and self.ocean_cpu_rect.collidepoint(event.pos):
                    x,y = self.pos_to_coords(event.pos, self.ocean_cpu_rect.topleft)
                    if x>= 0 and y >= 0 and x < self.rnd.width and y < self.rnd.width:
                        self.rnd.state.options['x'] = x
                        self.rnd.state.options['y'] = y
                        self.rnd.state.options['needs_redraw'] = True

            # keyboard command and position
            elif event.type == pygame.KEYDOWN and event.key in [pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT, pygame.K_RETURN, pygame.K_SPACE] and not self.debug:
                if event.key == pygame.K_RETURN:
                    self.rnd.state.options['ready'] = True
                    self.rnd.state.options['needs_redraw'] = True
                elif event.key == pygame.K_SPACE:
                    self.rnd.state.options['auto'] = True
                    self.rnd.state.options['needs_redraw'] = True
                elif event.key in [pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT]:
                    self.rnd.state.options['needs_redraw'] = True
                    x, y = self.rnd.state.options['x'], self.rnd.state.options['y']
                    if event.key == pygame.K_UP and self.rnd.state.options['y'] > 0:
                            self.rnd.state.options['y'] -= 1
                    elif event.key == pygame.K_DOWN and self.rnd.state.options['y'] < self.rnd.width - 1:
                            self.rnd.state.options['y'] += 1
                    elif event.key == pygame.K_LEFT and self.rnd.state.options['x'] > 0:
                            self.rnd.state.options['x'] -= 1
                    elif event.key == pygame.K_RIGHT and self.rnd.state.options['x'] < self.rnd.width - 1:
                            self.rnd.state.options['x'] += 1
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                self.rnd.state.switch(Stage.MENU, save=True)

class GuiGameOver:
    def __init__(self, rnd):
        self.rnd = rnd

        if self.rnd.state.options.get('victory', None) is None:
            self.rnd.state.options['victory'] = False

        # Precompute background
        if self.rnd.state.options.get('victory', False):
            slug = self.rnd.characters[self.rnd.player.character][1]
        else:
            slug = self.rnd.characters[self.rnd.cpu.character][1]
        self.bg = pygame.image.load("gfx/gameover_"+slug+".jpg").convert()


        ## Print outcome
        font_gameover = pygame.font.Font(FONT, 20)
        outcome_center = (WIDTH//2, HEIGHT//2 + 155)
        if self.rnd.state.options.get('victory', False):
            outcome_label = font_gameover.render(_("You have won."), 1, WHITE)
        else:
            outcome_label = font_gameover.render(_("You have lost."), 1, WHITE)
        outcome_rect  = outcome_label.get_rect()
        outcome_rect.center = outcome_center
        self.bg.blit(outcome_label, outcome_rect)


        ## Print score
        pos_score = (WIDTH//2, 598)
        font_score = pygame.font.Font(FONT, 12)

        ### Print players labels
        label_player = font_score.render(self.rnd.characters[self.rnd.player.character][0], 1, WHITE)
        p_rect = label_player.get_rect()
        p_rect.center = pos_score[0]-150, pos_score[1]-20

        label_enemy  = font_score.render(self.rnd.characters[self.rnd.cpu.character][0], 1, WHITE)
        c_rect = label_enemy.get_rect()
        c_rect.center = pos_score[0]+150, pos_score[1]-20

        self.bg.blit(label_player, p_rect)
        self.bg.blit(label_enemy,  c_rect)

        ### Print winner's coins
        coins = pygame.image.load("gfx/winner_coins_light.png").convert_alpha()
        coins_r = coins.get_rect()
        if self.rnd.state.options.get('victory', False):
            coins_r.midbottom = (pos_score[0] - 150, pos_score[1] - 15)
        else:
            coins_r.midbottom = (pos_score[0] + 150, pos_score[1] - 15)
        self.bg.blit(coins, coins_r)

        ### Print stats
        for (board, midtop) in [
                (self.rnd.cpu, (pos_score[0] - 150, pos_score[1] + 5)),
                (self.rnd.player, (pos_score[0] + 150, pos_score[1] + 5))
                ]:
            (x, y) = midtop
            for score in [
                    str(len(board.hits) + len(board.miss)) + " " + _("Shots"),
                    str(len(board.hits)) + " " + _("Hit"),
                    str(len(board.miss)) + " " + _("Miss"),
                    str(len(board.sunk)) + " " + _("Sunk"),
                    ]:
                label  = font_score.render(score, 1, WHITE)
                rect = label.get_rect()
                rect.midtop = (x, y+5)
                (x, y) = rect.midbottom
                self.bg.blit(label, rect)

        # Precompute menu
        self.menu_entries = []

        self.menu_entries.append(
                (_("Continue"), 'c', Stage.CREDITS, {})
                )

        ## Reference point for menu
        menu_bottomright = (WIDTH - 30, HEIGHT - 30)

        ## Font face/size for menu entries
        menu_font = pygame.font.Font(FONT, 20)

        ## Generate menu's sprites
        self.menu_sprites = []
        for n,(text, key, stage, opt) in enumerate(self.menu_entries):
            active   = menu_font.render(text, 1, ORANGE)
            inactive = menu_font.render(text, 1, WHITE)

            rect = active.get_rect()
            rect.bottomright = menu_bottomright

            self.menu_sprites.append((rect, inactive, active))

        # Initially select first entry
        self.menu_index = 0

    def update(self):
        pass

    def draw(self, s):
        update_rects = []

        # Draw background
        s.blit(self.bg, (0, 0))

        # Draw menu
        for (n, (rect, inactive, active)) in enumerate(self.menu_sprites):
            if self.menu_index == n:
                item = active
            else:
                item = inactive
            s.blit(item, rect)
            update_rects.append(rect)

        return update_rects

    def input(self, event):
        if event.type == pygame.MOUSEBUTTONDOWN:
            if self.menu_sprites[0][0].collidepoint(event.pos):
                self.rnd.state.options['skip'] = True
        elif event.type == pygame.KEYDOWN:
            if event.key in [pygame.K_RETURN, pygame.K_ESCAPE]:
                self.rnd.state.options['skip'] = True

class PyGameUI:
    def __init__(self):
        (language, encoding) = locale.getdefaultlocale()

        try:
            locale_dir = gettext.bindtextdomain("capbattleship")
        except:
            locale_dir = None
        finally:
            if locale_dir is None:
                locale_dir = "locale"

        try:
            tr = gettext.translation("capbattleship", localedir=locale_dir, languages=[language[0:2].lower()])
        except:
            tr = gettext.translation("capbattleship", localedir=locale_dir, languages=["en"])
        tr.install()

        # workaround: disable sound if sdl1 is used
        (a,b,c) = pygame.version.vernum
        if a == 1:
            pygame.mixer.pre_init(frequency=44100, size=-16, channels=2, buffer=512)
        pygame.init()
        icon = pygame.image.load("gfx/icon_256px.png")
        pygame.display.set_icon(icon)

        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((WIDTH,HEIGHT))

        pygame.display.set_caption(_("CAP - Pirate Battleship"))

        self.gui = None

        # Precompute notifications
        self.font_notifs = pygame.font.Font(FONT, 18)
        self.labels_notifs = {}
        for (msg,ctx) in [
                ("CPU hit you.",_("CPU hit you.")),
                ("CPU missed you.",_("CPU missed you.")),
                ("CPU sunk you.",_("CPU sunk you.")),
                ("You hit CPU.",_("You hit CPU.")),
                ("You missed CPU.",_("You missed CPU.")),
                ("You sunk CPU.",_("You sunk CPU.")),
                ("Already shot there.",_("Already shot there.")),
                ("CPU starts.",_("CPU starts.")),
                ("You start.",_("You start.")),
                ]:
            self.labels_notifs[msg] = self.font_notifs.render(ctx, 1, WHITE)

        maxw,maxh = max([l.get_width() for (k,l) in self.labels_notifs.items()]), max([l.get_height() for (k,l) in self.labels_notifs.items()])
        self.labels_rect = pygame.Rect((0,0), (maxw, maxh))
        self.labels_rect.center = (WIDTH//2, int(HEIGHT*0.85))

        pygame.mixer.music.load("music/scottbuckley_titan.ogg")
        pygame.mixer.music.set_volume(0.25)
        self.bgsound_state = None

        self.fullscreen = 0

        self.need_full_redraw = False
        self.last_state = None

    def draw_notification(self, s, message):
        notif_msg = self.labels_notifs[message]
        notif_rect = notif_msg.get_rect()
        notif_rect.center = self.labels_rect.center
        s.blit(notif_msg, notif_rect)
        return [self.labels_rect]

    def update(self, rnd):
        bgsound_param = rnd.params.get('music', None)
        if self.bgsound_state not in [BgRepeat.MENU, BgRepeat.ONCE] and (bgsound_param==BgRepeat.ONCE or bgsound_param==BgRepeat.MENU and rnd.state.stage in [Stage.MENU, Stage.CREDITS, Stage.OPTIONS]):
            self.bgsound_state = bgsound_param
            try:
                pos = pygame.mixer.music.get_pos()
            except:
                pos = 0
            pygame.mixer.music.play(loops=0, start=pos)
        elif self.bgsound_state!=BgRepeat.NONE and bgsound_param==BgRepeat.MENU and rnd.state.stage not in [Stage.MENU, Stage.CREDITS, Stage.OPTIONS]:
            self.bgsound_state = BgRepeat.NONE
            pygame.mixer.music.stop()
            pygame.mixer.music.rewind()
        elif self.bgsound_state!=BgRepeat.LOOP and bgsound_param==BgRepeat.LOOP:
            self.bgsound_state = bgsound_param
            try:
                pos = pygame.mixer.music.get_pos()
            except:
                pos = 0
            pygame.mixer.music.play(loops=-1, start=pos)
        elif self.bgsound_state!=BgRepeat.NONE and bgsound_param==BgRepeat.NONE:
            self.bgsound_state = bgsound_param
            pygame.mixer.music.stop()
            pygame.mixer.music.rewind()

        if self.last_state != rnd.state.stage:
            new_gui = None
            if rnd.state.stage == Stage.MENU and self.last_state != Stage.MENU:
                new_gui = GuiMenu(rnd)
            elif rnd.state.stage == Stage.CREDITS and self.last_state != Stage.CREDITS:
                new_gui = GuiCredits(rnd)
            elif rnd.state.stage == Stage.OPTIONS and self.last_state != Stage.OPTIONS:
                new_gui = GuiOptions(rnd)
            elif rnd.state.stage == Stage.CHARACTERS and self.last_state != Stage.CHARACTERS:
                new_gui = GuiSelection(rnd)
            elif rnd.state.stage in [Stage.PLAYER_SHIPS,Stage.CPU_SHIPS] and self.last_state not in [Stage.PLAYER_SHIPS,Stage.CPU_SHIPS]:
                new_gui = GuiSetupShips(rnd)
            elif rnd.state.stage in [Stage.PLAYER_TURN,Stage.CPU_TURN,Stage.NOTIFICATION] and self.last_state not in [Stage.PLAYER_TURN,Stage.CPU_TURN,Stage.NOTIFICATION]:
                new_gui = GuiBattleShips(rnd)
            elif rnd.state.stage == Stage.GAMEOVER and self.last_state != Stage.GAMEOVER:
                new_gui = GuiGameOver(rnd)

            if new_gui is not None:
                del self.gui
                self.gui=new_gui
                self.need_full_redraw = True
            self.last_state = rnd.state.stage

        if self.gui is not None:
            self.gui.update()

        if rnd.state.stage == rnd.State.Stage.NOTIFICATION:
            now = pygame.time.get_ticks()
            timeout = rnd.params.get('notif_timeout', None)
            t = rnd.state.options.get('timer', None)
            if t is None:
                rnd.state.options['timer'] = now
            elif timeout is not None and now - t > timeout:
                rnd.state.options['skip'] = True

    def draw(self, rnd):
        self.update(rnd)

        if rnd.params.get('fullscreen', 0)>0 and self.fullscreen!=rnd.params['fullscreen']:
            del self.screen
            self.screen = None
            (mx,my) = pygame.display.list_modes()[0]
            pygame.display.quit()
            if mx*HEIGHT == my*WIDTH:
                self.fs_off = (0, 0)
                self.fs_ratio = mx/WIDTH
            elif mx*HEIGHT > my*WIDTH:
                self.fs_off = (int((mx - my*WIDTH/HEIGHT)/2), 0)
                self.fs_ratio = my/HEIGHT
            else:
                self.fs_off = (0, int((my - mx/WIDTH*HEIGHT)/2))
                self.fs_ratio = mx/WIDTH
            pygame.display.init()
            if rnd.params['fullscreen']==2:
                flags = pygame.FULLSCREEN
            else:
                flags = 0
            self.screen2 = pygame.display.set_mode((mx, my), flags)
            self.screen2.fill(BLACK)
            self.screen = pygame.Surface((WIDTH,HEIGHT))
            self.fullscreen = rnd.params['fullscreen']
            self.need_full_redraw = True
        elif rnd.params.get('fullscreen', )==0 and self.fullscreen>0:
            if self.screen2 is not None:
                del self.screen2
                self.screen2 = None
            pygame.display.quit()
            pygame.display.init()
            self.screen = pygame.display.set_mode((WIDTH, HEIGHT))
            self.fullscreen = 0
            self.need_full_redraw = True

        update_rects = []

        if rnd.state.options.get('needs_redraw', True):
            update_rects += self.gui.draw(self.screen)
            if rnd.state.options.get("message", None) is not None and rnd.state.stage in [Stage.PLAYER_SHIPS,Stage.PLAYER_TURN,Stage.CPU_TURN,Stage.NOTIFICATION]:
                update_rects+=self.draw_notification(self.screen, rnd.state.options["message"])
            rnd.state.options['needs_redraw'] = False

        if self.fullscreen>0:
            big = pygame.transform.smoothscale(self.screen, (int(WIDTH*self.fs_ratio), int(HEIGHT*self.fs_ratio)))
            self.screen2.blit(big, self.fs_off)
            if self.need_full_redraw:
                pygame.display.update()
            else:
                new_rects = [pygame.Rect(
                    self.fs_off[0] + r.left*self.fs_ratio,
                    self.fs_off[1] + r.top*self.fs_ratio,
                    r.width*self.fs_ratio,
                    r.height*self.fs_ratio)
                    for r in update_rects]
                pygame.display.update(new_rects)
        elif self.need_full_redraw:
            pygame.display.update()
            self.need_full_redraw = False
        else:
            pygame.display.update(update_rects)
        self.clock.tick(30)


    def read_events(self, rnd):
        for event in pygame.event.get():

            if self.fullscreen>0 and event.type in [pygame.MOUSEMOTION, pygame.MOUSEBUTTONDOWN]:
                pos_x = int((event.pos[0]-self.fs_off[0])/self.fs_ratio)
                pos_y = int((event.pos[1]-self.fs_off[1])/self.fs_ratio)
                event.pos = (pos_x, pos_y)

            if event.type == pygame.QUIT or (event.type==pygame.KEYDOWN and event.unicode=='q'):
                rnd.state.switch(Stage.QUIT)

            elif event.type == pygame.ACTIVEEVENT or event.type == pygame.VIDEOEXPOSE:
                rnd.state.options['needs_redraw'] = True
                self.need_full_redraw = True
            elif rnd.state.stage == Stage.NOTIFICATION:
                if event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.KEYDOWN:
                    rnd.state.options['skip'] = True
            else:
                self.gui.input(event)
