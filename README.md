# CAP : Pirate Battleship

## Features

  * Battleship game
  * Pirate theme
  * Music, animations and sound effects
  * Safe for children
  * Language: English, French, German, Italian, Bokmål
  * Preferences saving in ```${XDG_CONFIG_HOME:-$HOME/.config}/capbattleship/conf```

## Installation

  * ArchLinux: [capbattleship-git](https://aur.archlinux.org/packages/capbattleship-git/) (AUR)
```shell
git clone https://aur.archlinux.org/capbattleship-git.git
cd capbattleship-git
makepkg -c -i -s
```
  * Debian/Ubuntu/etc.: [capbattleship_last_all.deb](https://chezlefab.net/share/3f6c697fa65faf0baece/capbattleship_last_all.deb) (packaging officiel en cours)
```shell
wget 'https://chezlefab.net/share/3f6c697fa65faf0baece/capbattleship_last_all.deb'
apt install ./capbattleship_last_all.deb
```
  * FlatPak:
```shell
# with or without --user (as a user or as root)
flatpak install --user net.tedomum.CapBattleship.flatpakref
flatpak run net.tedomum.CapBattleship
```
  * pip:
```shell
wget 'https://forge.tedomum.net/cap-pirate-battleship/capbattleship/-/archive/master/capbattleship-master.tar.gz'
tar xf capbattleship-master.tar.gz
cd capbattleship-master
pip install .
```
  * manual (linux) [tarball](https://forge.tedomum.net/cap-pirate-battleship/capbattleship/-/archive/master/capbattleship-master.tar.gz):
```shell
wget 'https://forge.tedomum.net/cap-pirate-battleship/capbattleship/-/archive/master/capbattleship-master.tar.gz'
tar xf capbattleship-master.tar.gz
capbattleship-master/capbattleship.py
```
  * standalone (windows, unsupported) [zip](https://chezlefab.net/share/3f6c697fa65faf0baece/capbattleship_last.zip)

## Dependencies:

  * python3 (works with >= 3.7.3) (uses os, configparser, random, enum, math, gettext, locale, glob)
  * python3-pygame (>= 1.9.4) or python3-pygame-sdl2 (>= 7.3.5)
  * python3-setuptools
  * python3-importlib-metadata or python3-pkg-resources

License: see LICENSE file

## Screenshots (from v1.0~alpha7)

![Main screen](https://forge.tedomum.net/cap-pirate-battleship/capbattleship/-/raw/master/screenshots/v1.0alpha7_main.jpg)

![A fierce battle is going on](https://forge.tedomum.net/cap-pirate-battleship/capbattleship/-/raw/master/screenshots/v1.0alpha7_battle.jpg)
