��    E      D  a   l      �     �     �                    *  
   >     I     Z     r          �     �  )   �     �     �     �                  
   ,     7     @     E  �   M  
     6        N     S     W  /   ^     �     �  	   �     �  %   �     �     �     �  	   	     	  �   	     �	     �	     �	     �	     �	     �	  D   
     F
  1   U
     �
      �
  	   �
     �
     �
     �
  �   �
  �   �     l     z          �     �     �     �  
   �     �  �  �     �     �     �     �     �  !   �  
   �                *     K     h  !   z  )   �     �     �     �     �            
         +     4     9  �   A  
   	  6        K     P     T  /   [     �     �  	   �     �  %   �     �     �     �  	         
  �   "     �     �     �          	       E        d  1   v     �     �  	   �     �     �     �  �     �   �     �     �     �     �     �  !   �  !   �  
     !         )      <       '          +   C   8   /         4                 3   2          *   >             .   ;                  B      "                 @             :   	      1      %                           ?       $                  &   =   #   6   -   0      ,   9   
       A       D   7   !       (   5                                        E          0.5s 100 Golden Hern piece (Coins) 1s 2s All the testers Already shot there. Animations Background Music CAP - Pirate Battleship CPU hit you. CPU missed you. CPU starts. CPU sunk you. Canon Gun - Canon sound_01 (sound effect) Cheat Choose your pirate Container Pack (Barrels) Continue Continue the game Contribute! Difficulty Disabled Easy Enabled Every shipman is only grousing day and night, which drives the captain completely mad. There is one thing you can do to help him unwind, that is be an easy pray. I would advise you not to… Fullscreen Graphism, game design (illustrations, ships, VFX, Map) Hard Hit Insane Join us on https://capbattleship.tuxfamily.org/ Manage options Menu Menu only Miss Mr. Bumble (Inspiration for 3D model) Normal Notification speed Pirate Battleship Play once Quit Rather than the vessel, 'The Princess' refers to its heartless captain. Famous buccaneer, she is said to be as rich as a king. Well, if she meets you, maybe she well become even richer… Repeat Restart the game See credits Sfx Shots Skip Source code development (uses python3, pygame), translation (fr, en) Start the game Stone in the water 080117-002 drop (sound effect) Sunk Thanks! (testing, advices, bugs) The Anger The Pirates Storm The Princess The Victory Saber They were twin brothers, incredibly talentuous swashbucklers, and the fell for a mysterious lady. She said only the most fearsome pirate would be her match. Will you try their cruelty? This vessel is known for its sneaky attacks during storms. Manœuvrable and fast, no ship can escape her in the raging waters. If the weather is taking a turn for the worse, be sure that you're alone… Titan (Music) Wait Windowed You have lost. You have won. You hit CPU. You missed CPU. You start. You sunk CPU. Project-Id-Version: 1.0
Report-Msgid-Bugs-To: contact@capbattleship.tuxfamily.org
PO-Revision-Date: 2020-11-27 01:28+0000
Last-Translator: Fabien Givors <captnfab+wl@chezlefab.net>
Language-Team: English <https://hosted.weblate.org/projects/capbattleship/capbattleship/en/>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Weblate 4.4-dev
 0.5s 100 Golden Hern piece (Coins) 1s 2s All the testers Captain, we already shot there… Animations Background Music CAP - Pirate Battleship The enemy hit one of your ships. The enemy missed your ships. The enemy starts. The enemy sank one of your ships. Canon Gun - Canon sound_01 (sound effect) Cheat Choose your pirate Container Pack (Barrels) Continue Resume the battle! Contribute! Difficulty Disabled Easy Enabled Every shipman is only grousing day and night, which drives the captain completely mad. There is but one thing you can do to help him unwind, and an easy prey it would be. I would advise you not to… Fullscreen Graphics, game design (illustrations, ships, VFX, map) Hard Hit Insane Join us on https://capbattleship.tuxfamily.org/ Manage options Menu Game menu Miss Mr. Bumble (Inspiration for 3D model) Normal Notification speed Pirate Battleship Play once Get back on terra firma Rather than the vessel, 'The Princess' refers to its heartless captain. Famous buccaneer, she is said to be as rich as a king. Well, if she meets you, maybe will she become even richer… Repeat Start a new battle! See credits SFX Canonballs shot Skip Source code development (uses Python 3, pygame), translation (fr, en) Start the battle! Stone in the water 080117-002 drop (sound effect) Sunk Thanks! (testing, advice, bugs) The Anger The Pirates Storm The Princess The Victory Saber They were twin brothers, incredibly talentuous swashbucklers, and the fell for a mysterious lady. She said only the most fearsome pirate would be her match. Will you try their cruelty? This vessel is known for its sneaky attacks during storms. Manœuvrable and fast, no ship can escape her in the raging waters. If the weather is taking a turn for the worse, be sure that you're alone… Titan (music) Wait Windowed You have lost. Congratulations, you won. You hit one of the enemy's ships. You hit one of the enemy's ships. You start. You sank one of the enemy's ship. 