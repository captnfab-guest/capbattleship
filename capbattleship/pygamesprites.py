from glob import glob
try:
    import pygame_sdl2 as pygame
    #raise ImportError
except ImportError:
    import pygame

class Sprite(pygame.sprite.Sprite):
    image_memoization = {}

    def draw(self, surface):
        if self.rect is not None:
            if self.image is not None:
                surface.blit(self.image, self.rect)
            return [self.rect]
        return []

    def _load_image(self, filename):
        memory = Sprite.image_memoization.get(filename, None)
        if memory is None:
            memory = pygame.image.load(filename).convert_alpha()
            if self.resize_width is not None:
                ratio = memory.get_height()/memory.get_width()
                memory = pygame.transform.scale(memory, (self.resize_width, int(ratio*self.resize_width)))
            Sprite.image_memoization[filename] = memory
        return memory

    def _update_rect(self):
        if self.image is not None and self.pos is not None:
            x, y = self.pos
            r = self.image.get_rect()
            if self.offset is not None:
                x, y = x + self.offset[0], y + self.offset[1]
            r.center = (x, y)
            self.rect = r
        else:
            r = None

    def __init__(self, fps=25, img_glob=None, resize_width=None, offset=None, pos=None, loop=False):
        super(Sprite, self).__init__()

        self.resize_width = resize_width
        self.images = []
        if img_glob is not None:
            for frame in sorted(glob(img_glob)):
                self.images.append(self._load_image(frame))

        if len(self.images) > 0:
            self.index = 0
        if self.index is None:
            self.image = None
        else:
            self.image = self.images[self.index]

        self.loop = loop

        self.rect = None
        self.offset = offset
        if pos is not None:
            self.set_pos(*pos)

        self.fps=fps
        self.last_update = None

        self.finished = False

    def set_pos(self, x, y):
        self.pos = (x,y)
        self._update_rect()

    def update(self):
        now = pygame.time.get_ticks()

        if self.index is None:
            self.finished = True

        if self.last_update is not None and (now-self.last_update)*self.fps >= 1000 and self.index is not None:
            self.last_update = now
            self.index += 1
        elif self.last_update is None:
            self.last_update = now

        if self.index is not None and self.index >= len(self.images):
            if self.loop:
                self.index = 0
            else:
                self.index  = None
                self.images = []
                self.image  = None

        if self.images is not None and self.index is not None:
            self.image = self.images[self.index]

class FxHit(Sprite):
    def __init__(self, *args, **kwargs):
        super(FxHit, self).__init__(img_glob="gfx/anime_fxhit_*.png", fps=6, *args, **kwargs)

class FxMiss(Sprite):
    def __init__(self, *args, **kwargs):
        super(FxMiss, self).__init__(img_glob="gfx/anime_fxmiss_*.png", fps=12, *args, **kwargs)

class ShipTurntable(Sprite):
    def __init__(self, ship_size, *args, **kwargs):
        super(ShipTurntable, self).__init__(img_glob="gfx/turntable_"+str(ship_size)+"cases/*", fps=12, *args, **kwargs)
