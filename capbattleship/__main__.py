#!/usr/bin/env python3
"""
CAP BattleShip, created by Fabien Givors and Damien Monteillard
"""

import sys, os, gettext

from .textui import DummyUI, TextUI
from .pygameui import PyGameUI

from .logic import Round

def game_main():
    game = Round(10, PyGameUI(), DummyUI())
    game.run()

def main():
    # system path for assets and locales, used if equivalent directories are not
    # present with this file
    sysprefix = "/usr"
    xdgprefix = os.getenv('XDG_DATA_HOME', os.path.expanduser('~/.local/share'))
    binprefix = os.path.split(os.path.abspath(sys.argv[0]))[0]

    if os.path.isdir(os.path.join(binprefix, "assets")):
        # Game is installed where launcher is
        assets_path = os.path.join(binprefix, "assets")
        locale_path = os.path.join(binprefix, "locale")

    elif os.path.isdir(os.path.join(xdgprefix, "games", "capbattleship", "assets")):
        # Game is installed in xdg path
        assets_path = os.path.join(xdgprefix, "games", "capbattleship", "assets")
        locale_path = os.path.join(xdgprefix, "locale")
    else:
        assets_path = os.path.join(sysprefix, "share", "games", "capbattleship", "assets")
        locale_path = os.path.join(sysprefix, "share", "locale")

    # update CWD and python include path
    os.chdir(assets_path)
    gettext.bindtextdomain("capbattleship", locale_path)

    game_main()

if __name__=="__main__":
    main()
