#!/usr/bin/env python3
from setuptools import setup
import os

datadirs = [
        (os.path.join('share', 'games', 'capbattleship'), 'assets'),
        (os.path.join('share' ), 'locale'),
        ]

exclude_ext = ['pot', 'po', 'po~' ]
datafiles = []
for dest,source in datadirs:
    datafiles += [
            (os.path.join(dest,d), [
                os.path.join(d,f) for f in files if not any(ext in exclude_ext for ext in f.split('.')[-1:])
                ]) for d, folders, files in os.walk(source)]

datafiles.append((os.path.join('share', 'applications'), [os.path.join('dist', 'net.tedomum.CapBattleship.desktop')]))
datafiles.append((os.path.join('share', 'icons', 'hicolor', '256x256', 'apps'), [os.path.join('dist', 'net.tedomum.CapBattleship.png')]))
datafiles.append((os.path.join('share', 'icons', 'hicolor', 'scalable', 'apps'), [os.path.join('dist', 'net.tedomum.CapBattleship.svg')]))
datafiles.append((os.path.join('share', 'metainfo'), [os.path.join('dist', 'net.tedomum.CapBattleship.metainfo.xml')]))

setup(
    name='capbattleship',
    version='0.0.8',
    description='A Pirate themed battleship game',
    url='https://forge.tedomum.net/captnfab/capbattleship/',
    author='Fabien Givors and Damien Monteillard',
    author_email='f+capbattleship@chezlefab.net',
    license='MIT',
    packages=['capbattleship'],
    python_requires='>=3.7',
    install_requires=['pygame>=1.9.4',
                      'setuptools>=40.8.0',
                      ],

    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: End Users/Desktop',
        'License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Games/Entertainment :: Board Games',
    ],
    data_files=datafiles,
    #scripts=["capbattleship.py"],
    entry_points={
        'gui_scripts': [
            'capbattleship=capbattleship.__main__:main',
            ]
        }
)
